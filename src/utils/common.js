// get all cookies
export const getCookie = (cname) => {
	var name = cname + '=';
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
};

// check login
export const isLogin = () => {
	if (getCookie('c_user') !== '') {
		return true;
	}

	return false;
};

// check object empty
export const isEmpty = (obj) => {
	for (let key in obj) {
		if (obj.hasOwnProperty(key)) return false;
	}
	return true;
};

export const checkExistCourseInCookie = (courseId) => {
	const courseCookie = getCookie('course_cart');
	if (courseCookie !== '') {
		if (courseCookie.includes(courseId)) {
			return true;
		}
	}

	return false;
};

export const setCookieCourseCart = (courseId) => {
	const courseCookie = getCookie('course_cart');
	let expires = '';
	let date = new Date();
	date.setTime(date.getTime() + 90 * 24 * 60 * 60 * 1000);
	expires = '; expires=' + date.toUTCString();
	if (courseCookie === '') {
		document.cookie = 'course_cart' + '=' + courseId + expires + '; path=/';
		return;
	}

	const courseCartResult = courseCookie.concat('-').concat(courseId);
	document.cookie =
		'course_cart' + '=' + courseCartResult + expires + '; path=/';
};

export const deleteCourseInCookieCourseCart = (courseId) => {
	const courseCookie = getCookie('course_cart');
	let expires = '';
	let date = new Date();
	date.setTime(date.getTime() + 90 * 24 * 60 * 60 * 1000);
	expires = '; expires=' + date.toUTCString();

	const courseCookieSplit = courseCookie.split('-');
	const index = courseCookieSplit.findIndex((course) => course === courseId);

	courseCookieSplit.splice(index, 1);
	const newCourseCookie = courseCookieSplit.join('-');

	document.cookie =
		'course_cart' + '=' + newCourseCookie + expires + '; path=/';
};

export const deleteAllCookieCart = () => {
	document.cookie = 'course_cart= ; expires = Thu, 01 Jan 1970 00:00:00 GMT';
};
