import React, { createContext } from 'react';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ToastifyContext = createContext(null);

export { ToastifyContext };

export default ({ children }) => {
	const notify = (type, text) => {
		switch (type) {
			case 'success':
				toast.success(text);
				break;
			case 'info':
				toast.info(text);
				break;
			case 'warning':
				toast.warn(text);
				break;
			case 'error':
				toast.error(text);
				break;
			default:
				toast.dark(text);
				break;
		}
	};

	return (
		<ToastifyContext.Provider value={notify}>
			<ToastContainer
				position='top-right'
				autoClose={2000}
				hideProgressBar={false}
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss={false}
				draggable
				pauseOnHover={false}
			/>
			{children}
		</ToastifyContext.Provider>
	);
};
