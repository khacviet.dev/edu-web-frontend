import { Button, Divider, Grid, Typography } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import VideocamIcon from '@material-ui/icons/Videocam';
import courseApi from 'api/courseApi';
import userInfoApi from 'api/userInfoApi';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { getCookie } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

function LearnCourse(props) {
	const notify = useContext(ToastifyContext);
	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.down('sm'));
	const [isLoading, setIsLoading] = useState(false);
	const [source, setSource] = useState({});
	const [videoId, setVideoId] = useState('');
	const [sourceId, setSourceId] = useState();
	const [lessonId, setLessonId] = useState();

	const history = useHistory();
	const [listLesson, setListLesson] = useState([]);
	const [expanded, setExpanded] = React.useState('');
	const state = props.location.state;
	const refer = [
		{
			display: 'Khóa học',
			value: 'course',
		},
		{
			display: `${state ? state.course_title : ''}`,
			value: 'coursedetail',
			course_id: state.course_id,
		},
	];
	const handleChange = (panel) => (event, newExpanded) => {
		setExpanded(newExpanded ? panel : false);
	};

	useEffect(() => {
		window.scrollTo({ top: 0 });
		setSourceId(state.source_id);
		setLessonId(state.lesson_id);
	}, []);

	if (state != undefined) {
		// get list lesson
		useEffect(() => {
			const data = {
				course_id: state.course_id,
			};
			setIsLoading((loading) => !loading);
			const user = getCookie('c_user');
			if (user) {
				const getLessonList = async () => {
					try {
						const response = await userInfoApi.getLessonList(data);
						setListLesson(response.data);
						setIsLoading((loading) => !loading);
					} catch (error) {
						setIsLoading((loading) => !loading);
						notify('error', error.response.data.message);
					}
				};
				getLessonList();
			} else {
				const getLessonList = async () => {
					try {
						const response = await courseApi.getLessonList(data);
						setListLesson(response.data);
						setTotalLesson(response.total_lesson);
						setIsLoading((loading) => !loading);
					} catch (error) {
						setIsLoading((loading) => !loading);
						notify('error', error.response.data.message);
					}
				};
				getLessonList();
			}
		}, []);

		// get source detail
		useEffect(() => {
			const params = {
				source_id: sourceId,
				lesson_id: lessonId,
			};
			setIsLoading((loading) => !loading);
			const getLessonSource = async () => {
				try {
					const response = await courseApi.getLessonSource(params);
					const data = response.data;
					if (data.length > 0) {
						setSource(data[0]);
						setVideoId(data[0].source_link);
					}
					setIsLoading((loading) => !loading);
				} catch (error) {
					setIsLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getLessonSource();
		}, [sourceId]);
	} else {
		history.push({
			pathname: '/',
		});
	}

	const handleStartLearn = (source_id, index1, lesson_title, lesson_id) => {
		setLessonId(lesson_id);
		setSourceId(source_id);
	};

	function getId(url) {
		const regExp =
			/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
		const match = url.match(regExp);

		return match && match[2].length === 11 ? match[2] : null;
	}

	return (
		<MasterLayout
			breadcrumbs={matches ? false : true}
			referer={refer}
			header={`${source.source_title} (${source.lesson_title})`}
		>
			{isLoading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Vào học | Xoài Education</title>
			</Helmet>
			{source && (
				<Grid>
					<Grid container style={{ marginTop: '2rem' }}>
						<Grid item md={12}>
							<Typography style={{ fontSize: '1.5rem', color: '#1E90FF' }}>
								Bài {source.order} : {source.source_title}
							</Typography>
						</Grid>
						<Grid container item md={12} style={{ position: 'relative' }}>
							<Grid item md={8} style={{ width: '100%' }}>
								<iframe
									style={{
										width: matches ? '100%' : '800px',
										height: matches ? 'auto' : '480px',
									}}
									// src={`https://www.youtube.com/embed/${videoId}`}
									src={`${videoId}`}
									frameBorder='0'
									allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
									allowFullScreen
									title='Embedded youtube'
								></iframe>
							</Grid>
							<Grid item md={4} style={{ marginTop: matches ? '1rem' : '0' }}>
								{listLesson.map((data, index) => (
									<Accordion
										square
										key={`lesson${index}`}
										expanded={expanded === data.lesson_id}
										onChange={handleChange(data.lesson_id)}
									>
										<AccordionSummary
											expandIcon={<ExpandMoreIcon />}
											style={{ cursor: 'pointer' }}
										>
											<Typography variant='body1'>
												{data.lesson_title}
											</Typography>
										</AccordionSummary>
										<AccordionDetails>
											<Grid container>
												{data.list_source.map((source, index1) => (
													<Grid
														key={`source${index1}`}
														container
														style={{ marginTop: '1rem' }}
													>
														<Grid item md={8}>
															<Grid container alignItems='center'>
																<VideocamIcon color='action' />
																<Typography variant='body1'>
																	Bài {source.source_order}:&nbsp;
																	{source.source_title}
																</Typography>
															</Grid>
														</Grid>
														{/* <Grid item md={2} container justify='flex-end'>
														{source.source_duration != null
															? convertHMS(source.source_duration)
															: ''}
													</Grid> */}
														<Grid item md={4} container justify='flex-end'>
															{sourceId == source.source_id ? (
																<Button
																	size='small'
																	variant='contained'
																	color='primary'
																	disabled
																>
																	Đang học
																</Button>
															) : (
																<Button
																	size='small'
																	variant='contained'
																	color='primary'
																	onClick={() =>
																		handleStartLearn(
																			source.source_id,
																			index1,
																			data.lesson_title,
																			data.lesson_id
																		)
																	}
																>
																	Bắt đầu
																</Button>
															)}
														</Grid>
													</Grid>
												))}
											</Grid>
										</AccordionDetails>
									</Accordion>
								))}
							</Grid>
						</Grid>
					</Grid>
					<Grid container>
						<Divider style={{ width: '100%' }} />
					</Grid>
					<Grid container style={{ marginTop: matches ? '1rem' : '0' }}>
						<Grid item md={12}>
							<Typography style={{ fontSize: '1.5rem', color: '#1E90FF' }}>
								Nội dung bài học:
							</Typography>
						</Grid>

						<Grid item md={12}>
							<Typography style={{ fontSize: '1.2rem' }}>
								<div
									dangerouslySetInnerHTML={{ __html: source.source_detail }}
								></div>
							</Typography>
						</Grid>
					</Grid>
				</Grid>
			)}
		</MasterLayout>
	);
}

export default LearnCourse;
