import {
	Avatar,
	Button,
	Divider,
	Grid,
	Paper,
	Typography,
} from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
import CallIcon from '@material-ui/icons/Call';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import VideocamIcon from '@material-ui/icons/Videocam';
import classApi from 'api/classApi';
import courseApi from 'api/courseApi';
import userInfoApi from 'api/userInfoApi';
import RegisterForm from 'common/components/RegisterForm';
import ReportDialog from 'common/components/ReportDialog';
import MasterLayout from 'common/pages/MasterLayout';
import { addToCart } from 'features/Course/courseSlice';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import {
	checkExistCourseInCookie,
	getCookie,
	isLogin,
	setCookieCourseCart,
} from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';
import './CourseDetail.css';

function CourseDetail(props) {
	const notify = useContext(ToastifyContext);
	const history = useHistory();
	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.down('sm'));
	useEffect(() => {
		window.scrollTo({ top: 0 });
	}, []);
	const state = props.location.state;
	const [listLesson, setListLesson] = useState([]);
	const [course, setCourse] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [currentCost, setCurrentCost] = useState(0);
	const [totalLesson, setTotalLesson] = useState(0);
	const [expanded, setExpanded] = React.useState('');
	const [isBuy, setIsBuy] = useState();

	const refer = [
		{
			display: 'Khóa học',
			value: 'course',
		},
	];

	// check state
	if (state != undefined) {
		const user_id = getCookie('c_user');

		useEffect(() => {
			if (user_id) {
				const params = {
					course_id: state.course_id,
				};
				setIsLoading((loading) => !loading);
				const checkIsBuy = async () => {
					try {
						const response = await userInfoApi.checkIsBuy(params);

						const data = response.messsage;
						if (data == false) {
							setIsBuy(false);
						} else {
							setIsBuy(true);
						}

						setIsLoading((loading) => !loading);
					} catch (error) {
						setIsLoading((loading) => !loading);
						notify('error', error.response.data.message);
					}
				};
				checkIsBuy();
			} else {
				setIsBuy(false);
			}

			if (state.lesson_id) {
				setExpanded(state.lesson_id);
			}
		}, []);

		useEffect(() => {
			const params = {
				course_id: state.course_id,
			};
			setIsLoading((loading) => !loading);
			const getCourse = async () => {
				try {
					const response = await courseApi.getCourse(params);
					const data = response.data;
					setCourse(data[0]);

					setCurrentCost(
						BigInt(data[0].course_cost) - BigInt(data[0].discount)
					);
					setIsLoading((loading) => !loading);
				} catch (error) {
					setIsLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getCourse();
		}, []);

		// get lít lesson
		useEffect(() => {
			const data = {
				course_id: state.course_id,
			};
			setIsLoading((loading) => !loading);
			const user = getCookie('c_user');
			if (user) {
				const getLessonList = async () => {
					try {
						const response = await userInfoApi.getLessonList(data);
						// const data = response.data;
						// setListSubject(data);
						setListLesson(response.data);
						setTotalLesson(response.total_lesson);
						setIsLoading((loading) => !loading);
					} catch (error) {
						setIsLoading((loading) => !loading);
						notify('error', error.response.data.message);
					}
				};
				getLessonList();
			} else {
				const getLessonList = async () => {
					try {
						const response = await courseApi.getLessonList(data);
						// const data = response.data;
						// setListSubject(data);
						setListLesson(response.data);
						setTotalLesson(response.total_lesson);
						setIsLoading((loading) => !loading);
					} catch (error) {
						setIsLoading((loading) => !loading);
						notify('error', error.response.data.message);
					}
				};
				getLessonList();
			}
		}, []);
	} else {
		history.push('/NotFound');
		// chuyển đến trang note found
	}

	const handleChange = (panel) => (event, newExpanded) => {
		setExpanded(newExpanded ? panel : false);
	};
	// register
	const handleRegis = (e) => {
		e.preventDefault();
		const request = {
			name: e.target.name.value,
			email: e.target.email.value,
			phone: e.target.phone.value,
			note: e.target.note.value,
			course_id: course.id,
			user_id: getCookie('c_user') || null,
		};
		const insertRegister = async () => {
			setIsLoading((loading) => !loading);
			try {
				const response = await classApi.insertRegister(request);

				// const data = response.data;
				// setListSubject(data);
				// setListClass(data);

				// setTotalRow(response.data[0].count);
				setIsLoading((loading) => !loading);
				notify('success', response.message);
			} catch (error) {
				setIsLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		insertRegister();
	};

	// convert time to HH:MM:SS
	function convertHMS(value) {
		const sec = parseInt(value, 10); // convert value to number if it's string
		let hours = Math.floor(sec / 3600); // get hours
		let minutes = Math.floor((sec - hours * 3600) / 60); // get minutes
		let seconds = sec - hours * 3600 - minutes * 60; //  get seconds
		// add 0 if value < 10; Example: 2 => 02

		if (hours < 10) {
			hours = '0' + hours;
		}
		if (minutes < 10) {
			minutes = '0' + minutes;
		}
		if (seconds < 10) {
			seconds = '0' + seconds;
		}
		return (
			(hours != '00' ? hours + ':' : '') +
			(minutes != '00' ? minutes + ':' : '') +
			seconds
		); // Return is HH : MM : SS
	}

	// button start control
	const handleStartLearn = (source_id, index1, lesson_title, lesson_id) => {
		if (!isBuy) {
			notify('error', 'Vui lòng mua khóa học để có thể xem tiếp');
		} else {
			history.push({
				pathname: `/learn`,
				state: {
					source_id: source_id,
					course_title: course.title,
					course_id: course.id,
					lesson_title: lesson_title,
					lesson_id: lesson_id,
				},
			});
		}
	};

	const dispatch = useDispatch();

	const handleAddToCartClick = (courseId) => {
		if (!isLogin()) {
			notify('error', 'Vui lòng đăng nhập !');
			localStorage.setItem('courseLink', state.course_id);
			history.push('/signin');
			return;
		}
		if (checkExistCourseInCookie(courseId)) {
			notify('error', 'Khóa học đã tồn tại trong giỏ');
			return;
		}
		setCookieCourseCart(courseId);
		dispatch(addToCart());
		notify('success', 'Thêm vào giỏ thành công');
	};

	return (
		<MasterLayout
			breadcrumbs={matches ? false : true}
			referer={refer}
			header={
				course.title && course.title.length > 105
					? course.title.substr(0, 105) + '...'
					: course.title
			}
		>
			{isLoading == true && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			{course.title && (
				<Helmet>
					<title>{course.title} | Khóa học | Xoài Education</title>
				</Helmet>
			)}
			<Grid
				container
				spacing={3}
				style={{ marginTop: '1rem', marginBottom: ' 5rem' }}
			>
				<Grid item md={8}>
					<Paper>
						<Grid container style={{ padding: '1rem 1.5rem' }}>
							<Grid item sm={12} md={11}>
								<Typography style={{ fontSize: '1.5rem' }}>
									{course.title}
								</Typography>
							</Grid>
							<Grid
								container
								justify={matches ? 'flex-start' : 'flex-end'}
								style={{ marginTop: matches ? '0.5rem' : '0' }}
								item
								sm={12}
								md={1}
							>
								{isBuy && (
									<ReportDialog
										courseId={course.id}
										className={course.title}
										icon={'2'}
									/>
								)}
							</Grid>
						</Grid>
						<Grid container>
							<Divider style={{ width: '100%' }} />
						</Grid>

						<Grid
							container
							direction='column'
							style={{ padding: '1rem 1.5rem' }}
						>
							<div
								dangerouslySetInnerHTML={{ __html: course.description }}
							></div>
							<Typography
								style={{
									fontSize: '1.25rem',
									fontWeight: 'bold',
									marginTop: '2rem',
									marginBottom: '1rem',
								}}
							>
								Giáo trình
							</Typography>
							{listLesson.map((data, index) => (
								<Accordion
									square
									key={`lesson${index}`}
									expanded={expanded === data.lesson_id}
									onChange={handleChange(data.lesson_id)}
								>
									<AccordionSummary
										expandIcon={<ExpandMoreIcon />}
										style={{ cursor: 'pointer' }}
									>
										<Typography variant='body1'>{data.lesson_title}</Typography>
									</AccordionSummary>
									<AccordionDetails>
										<Grid container>
											{data.list_source.map((source, index1) => (
												<Grid
													key={`source${index1}`}
													container
													style={{ marginTop: '1rem' }}
													alignItems='center'
												>
													<Grid item md={8}>
														<Grid container alignItems='center'>
															<VideocamIcon color='action' />
															<Typography variant='body1'>
																Bài {source.source_order}:&nbsp;
																{source.source_title}
															</Typography>
														</Grid>
													</Grid>
													<Grid item md={2}>
														{source.source_duration != null
															? convertHMS(source.source_duration)
															: ''}
													</Grid>
													<Grid item md={2}>
														<Grid container justify='flex-end'>
															<Button
																size='small'
																variant='contained'
																color='primary'
																onClick={() =>
																	handleStartLearn(
																		source.source_id,
																		index1,
																		data.lesson_title,
																		data.lesson_id
																	)
																}
															>
																Bắt đầu
															</Button>
														</Grid>
													</Grid>
												</Grid>
											))}
										</Grid>
									</AccordionDetails>
								</Accordion>
							))}
						</Grid>
					</Paper>
				</Grid>

				<Grid item md={4}>
					<Grid container>
						<Grid item md={12}>
							<Card style={{ width: '100%' }}>
								<CardActionArea>
									<img
										src={course.thumbnail}
										style={{ width: '100%', height: 'auto' }}
										loading='lazy'
									/>
									<CardContent>
										<Typography gutterBottom variant='h6'>
											{course.title}
										</Typography>
										<Typography
											variant='body2'
											color='textSecondary'
											component='p'
										>
											{course.brief_info}
										</Typography>
									</CardContent>
								</CardActionArea>
								<CardActions>
									<Grid container alignItems='center'>
										{!isBuy && (
											<Grid item md={6} style={{ paddingLeft: '1rem' }}>
												<Typography
													variant='body1'
													style={{ fontSize: '1.5rem', color: 'blue' }}
												>
													{new Intl.NumberFormat('vi-VN', {
														style: 'currency',
														currency: 'VND',
													}).format(currentCost)}
												</Typography>
											</Grid>
										)}

										{!isBuy && (
											<Grid item md={6} style={{ paddingRight: '0.5rem' }}>
												<Typography
													color='primary'
													variant='body1'
													style={{
														fontSize: '1.25rem',
														textDecorationLine: 'line-through',
													}}
												>
													{course.discount != '0' &&
														new Intl.NumberFormat('vi-VN', {
															style: 'currency',
															currency: 'VND',
														}).format(course.course_cost)}
												</Typography>
											</Grid>
										)}

										<Grid item md={12} style={{ padding: '1rem' }}>
											<Grid container alignItems='center'>
												<BookmarkBorderIcon />
												<Typography
													style={{ fontWeight: 'bold' }}
													variant='body1'
												>
													&nbsp;{listLesson.length} phần, {totalLesson} bài học
												</Typography>
											</Grid>
										</Grid>
										<Grid item md={12} style={{ margin: '2rem 0 1.5rem 0' }}>
											{!isBuy && (
												<Grid
													container
													justify='center'
													style={{ width: '100%' }}
												>
													<Grid
														item
														md={6}
														style={{
															paddingRight: '0.5rem',
															paddingLeft: matches ? '0' : '1rem',
														}}
													>
														<RegisterForm
															handleRegis={handleRegis}
															buttonText='Nhận tư vấn'
															buttonVariant='contained'
															buttonColor='primary'
															icon={<CallIcon></CallIcon>}
														></RegisterForm>
													</Grid>
													<Grid item md={6}>
														<Button
															startIcon={<AddShoppingCartIcon />}
															variant='contained'
															color='secondary'
															onClick={() => handleAddToCartClick(course.id)}
														>
															Thêm vào giỏ
														</Button>
													</Grid>
												</Grid>
											)}
										</Grid>
									</Grid>
								</CardActions>
							</Card>
						</Grid>
						<Grid item md={12} style={{ marginTop: '2rem', width: '100%' }}>
							<Paper>
								<Grid container style={{ padding: '0.5rem 1rem' }}>
									<Typography variant='body1' style={{ fontSize: '1.25rem' }}>
										Giảng viên
									</Typography>
								</Grid>
								<Grid container>
									<Divider style={{ width: '100%' }} />
								</Grid>
								<Grid container style={{ padding: '0.5rem 1rem 2rem 1rem' }}>
									<Grid
										container
										alignItems='center'
										direction='column'
										style={{ marginTop: '0.5rem' }}
									>
										<Avatar
											style={{ width: '5rem', height: 'auto' }}
											alt='a'
											src={course.avt_link}
										/>
										<Typography style={{ marginTop: '0.5rem' }} variant='body1'>
											{course.teacher_name}
										</Typography>
									</Grid>
									<Grid container justify='center'>
										<Typography style={{ marginTop: '1rem' }} variant='body2'>
											{course.short_description}
										</Typography>
									</Grid>
								</Grid>
							</Paper>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</MasterLayout>
	);
}

export default CourseDetail;
