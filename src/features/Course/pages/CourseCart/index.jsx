import { Button, Grid, TextField, Typography } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import DeleteIcon from '@material-ui/icons/Delete';
import SettingsIcon from '@material-ui/icons/Settings';
import paymentApi from 'api/paymentApi';
import userInfoApi from 'api/userInfoApi';
import MasterLayout from 'common/pages/MasterLayout';
import { deleteToCart, setDefault } from 'features/Course/courseSlice';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import {
	deleteAllCookieCart,
	deleteCourseInCookieCourseCart,
	getCookie,
	isLogin,
} from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

CourseCart.propTypes = {};

function CourseCart(props) {
	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.down('sm'));
	const dispatch = useDispatch();
	const notify = useContext(ToastifyContext);

	const [loading, setLoading] = useState(false);
	const [isPayment, setIsPayment] = useState(false);
	const [isDone, setIsDone] = useState(false);
	const [user, setUser] = useState({});
	const [isDelete, setIsDelete] = useState(false);
	const [courses, setCourses] = useState([]);
	const [total, setTotal] = useState(-1);
	let history = useHistory();

	const handleCourseRouterChange = () => {
		history.push('/course');
	};

	const deleteCourseCart = (courseId) => {
		deleteCourseInCookieCourseCart(courseId);
		setIsDelete(!isDelete);
		notify('error', 'Xóa khỏi giỏ hàng thành công');
		dispatch(deleteToCart());
	};

	const handleOpenPayment = () => {
		setIsPayment(true);
	};

	const handleOpenCart = () => {
		setIsPayment(false);
	};

	const handlePaymentCourse = () => {
		const post = async () => {
			try {
				setLoading(true);
				const response = await paymentApi.paymentCourse({ courses });
				setLoading(false);
				notify('success', response.message);
				deleteAllCookieCart();
				dispatch(setDefault());
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};

		post();
	};

	useEffect(() => {
		const get = async () => {
			try {
				setLoading(true);
				const courseCart = getCookie('course_cart');
				const params = {
					courseCart,
				};
				const response = await paymentApi.getCourseByCourseCart(params);
				setLoading(false);
				setIsDone(true);
				setCourses(response.courses);
				setTotal(response.total);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};

		get();
	}, [isDelete]);

	useEffect(() => {
		if (isLogin()) {
			const getUserInfo = async () => {
				try {
					setLoading(true);
					const response = await userInfoApi.getUserInfo();
					const data = response.data;
					setUser(data[0]);
					setLoading(false);
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
				}
			};
			getUserInfo();
		}
	}, []);

	return (
		<MasterLayout>
			{loading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Giỏ hàng | Xoài Education</title>
			</Helmet>
			{!isPayment ? (
				<Grid container style={{ marginBottom: '3rem' }}>
					{courses.length > 0 && isDone ? (
						<Grid container spacing='3'>
							<Grid item md={12}>
								<Typography style={{ fontWeight: 'bold', fontSize: '1.25rem' }}>
									Giỏ hàng
								</Typography>
							</Grid>
							<Grid item md={9}>
								<Grid container>
									{courses.map((course, index) =>
										matches ? (
											<Grid
												key={index}
												container
												style={{ marginBottom: '1.5rem' }}
											>
												<Grid item md={3}>
													<img
														style={{
															height: '100%',
															width: matches ? '100%' : '229.5px',
														}}
														src={course.thumbnail}
														alt='Thumbnail'
														loading='lazy'
													/>
												</Grid>
												<Grid
													item
													md={9}
													style={{
														padding: '1rem 1.5rem 0.5rem 1.5rem',
														border: '1px solid #ccc',
														width: '100%',
													}}
												>
													<Grid
														container
														style={{ height: '100%', width: '100%' }}
													>
														<Grid item md={8}>
															<Grid container style={{ height: '100%' }}>
																<Grid container>
																	<Typography style={{ fontSize: '1.25rem' }}>
																		{course.title}
																	</Typography>
																</Grid>
																<Grid container alignItems='flex-end'>
																	<Button
																		color='secondary'
																		startIcon={<DeleteIcon />}
																		onClick={deleteCourseCart}
																	>
																		Xóa
																	</Button>
																</Grid>
															</Grid>
														</Grid>
														<Grid item md={1}></Grid>
														<Grid item md={3} container justify='flex-end'>
															<Typography
																style={{
																	fontSize: '1.25rem',
																	color: '#F7AB00',
																}}
															>
																{course.cost_new.toLocaleString('vi-VN', {
																	style: 'currency',
																	currency: 'VND',
																})}
																<br />
																{course.cost !== course.cost_new && (
																	<span
																		style={{
																			fontSize: '1rem',
																			color: '#ccc',
																			textDecoration: 'line-through',
																		}}
																	>
																		{course.cost.toLocaleString('vi-VN', {
																			style: 'currency',
																			currency: 'VND',
																		})}
																	</span>
																)}
															</Typography>
														</Grid>
													</Grid>
												</Grid>
											</Grid>
										) : (
											<Grid
												key={index}
												container
												style={{ marginBottom: '1.5rem', height: '200px' }}
											>
												<Grid item md={3}>
													<img
														style={{
															width: matches ? '100%' : '100%',
															height: '200px',
														}}
														src={course.thumbnail}
														alt='Thumbnail'
														loading='lazy'
													/>
												</Grid>
												<Grid
													item
													md={9}
													style={{
														padding: '1rem 1.5rem 0.5rem 1.5rem',
														border: '1px solid #ccc',
														height: '200px',
													}}
												>
													<Grid container style={{ height: '100%' }}>
														<Grid item md={8}>
															<Grid container style={{ height: '100%' }}>
																<Grid container>
																	<Typography style={{ fontSize: '1.25rem' }}>
																		{course.title}
																	</Typography>
																</Grid>
																<Grid container alignItems='flex-end'>
																	<Button
																		color='secondary'
																		startIcon={<DeleteIcon />}
																		onClick={() =>
																			deleteCourseCart(course.course_id)
																		}
																	>
																		Xóa
																	</Button>
																</Grid>
															</Grid>
														</Grid>
														<Grid item md={1}></Grid>
														<Grid item md={3} container justify='flex-end'>
															<Typography
																style={{
																	fontSize: '1.25rem',
																	color: '#F7AB00',
																}}
															>
																{course.cost_new.toLocaleString('vi-VN', {
																	style: 'currency',
																	currency: 'VND',
																})}
																<br />
																{course.cost !== course.cost_new && (
																	<span
																		style={{
																			fontSize: '1rem',
																			color: '#ccc',
																			textDecoration: 'line-through',
																		}}
																	>
																		{course.cost.toLocaleString('vi-VN', {
																			style: 'currency',
																			currency: 'VND',
																		})}
																	</span>
																)}
															</Typography>
														</Grid>
													</Grid>
												</Grid>
											</Grid>
										)
									)}
								</Grid>
							</Grid>
							<Grid
								item
								md={3}
								style={{
									width: '100%',
								}}
							>
								<Grid
									container
									style={{
										border: '1px solid #ccc',
										borderRadius: '5px',
										padding: '1.5rem',
									}}
									justify='center'
								>
									<Typography style={{ fontSize: '1.25rem' }}>
										Tạm tính: &nbsp;&nbsp;&nbsp;
										<span style={{ color: '#3C78E8', fontWeight: 'bold' }}>
											{total.toLocaleString('vi-VN', {
												style: 'currency',
												currency: 'VND',
											})}
										</span>
									</Typography>
								</Grid>
								<Grid container style={{ marginTop: '0.5rem' }}>
									<Button
										variant='contained'
										fullWidth
										style={{
											backgroundColor: '#5086EA',
											color: '#fff',
											textTransform: 'none',
											fontSize: '1.1rem',
											marginBottom: '2rem',
										}}
										onClick={handleOpenPayment}
									>
										Tiến hành thanh toán
									</Button>
								</Grid>
							</Grid>
						</Grid>
					) : (
						isDone && (
							<Grid container justify='center'>
								<Grid container justify='center'>
									<Typography variant='h6'>
										Không có khoá học nào trong giỏ hàng của bạn
									</Typography>
								</Grid>
								<Grid container justify='center' style={{ marginTop: '1rem' }}>
									<Button
										variant='contained'
										color='secondary'
										onClick={handleCourseRouterChange}
									>
										Xem các khóa học
									</Button>
								</Grid>
							</Grid>
						)
					)}
				</Grid>
			) : (
				<Grid container style={{ marginBottom: '3rem' }}>
					<Grid container>
						<Typography style={{ fontSize: '1.25rem', fontWeight: 'bold' }}>
							Thanh toán
						</Typography>
					</Grid>
					<Grid container>
						<Typography style={{ fontWeight: 'bold', marginTop: '0.5rem' }}>
							1. Thông tin tài khoản
						</Typography>
					</Grid>
					<Grid container style={{ marginTop: '0.5rem' }}>
						<Grid item md={8}>
							<Grid container>
								<Grid
									item
									md={12}
									style={{
										border: '1px solid #ccc',
										borderRadius: '5px',
										padding: '1rem',
									}}
								>
									<Grid container spacing={3}>
										<Grid item md={4}>
											<TextField
												label='Họ và tên'
												margin='dense'
												size='small'
												fullWidth
												disabled
												value={user.full_name}
											/>
										</Grid>
										<Grid item md={4}>
											<TextField
												label='Email'
												margin='dense'
												size='small'
												fullWidth
												disabled
												value={user.email}
											/>
										</Grid>
										<Grid item md={4}>
											<TextField
												label='Số điên thoại'
												margin='dense'
												size='small'
												fullWidth
												disabled
												value={user.phone}
											/>
										</Grid>
									</Grid>
								</Grid>
							</Grid>
							<Grid container style={{ marginTop: '1rem' }}>
								<Grid item md={12}>
									<Typography
										style={{ fontWeight: 'bold', marginTop: '0.5rem' }}
									>
										2. Phương thức thanh toán
									</Typography>
								</Grid>
								<Grid
									item
									md={12}
									style={{
										border: '1px solid #ccc',
										borderRadius: '5px',
										padding: '1rem',
										marginTop: '0.5rem',
									}}
								>
									<Grid container>
										<Grid
											container
											alignItems='center'
											style={{ marginBottom: '1rem' }}
										>
											<AccountCircleIcon
												style={{ color: '#3C78E8', marginRight: '0.5rem' }}
											/>
											<Typography style={{ color: '#3C78E8' }}>
												Chủ tài khoản: Võ Khắc Việt
											</Typography>
										</Grid>
										<Grid
											container
											alignItems='center'
											style={{ marginBottom: '1rem' }}
										>
											<CreditCardIcon
												style={{ color: '#3C78E8', marginRight: '0.5rem' }}
											/>
											<Typography style={{ color: '#3C78E8' }}>
												Số tài khoản: 02608081801
											</Typography>
										</Grid>
										<Grid
											container
											alignItems='center'
											style={{ marginBottom: '1rem' }}
										>
											<AccountBalanceIcon
												style={{ color: '#3C78E8', marginRight: '0.5rem' }}
											/>
											<Typography style={{ color: '#3C78E8' }}>
												Ngân hàng TPBank chi nhánh Hà Nội
											</Typography>
										</Grid>
										<Grid container style={{ marginBottom: '1rem' }}>
											<Typography>
												{
													'Nội dung chuyển khoản: Nộp học phí - <khóa học> - <họ tên> - <số di động>'
												}
											</Typography>
										</Grid>
										<Grid container>
											<Typography>
												{
													'Khóa học sẽ được kích hoạt sau khi Xoaimaster kiểm tra tài khoản và xác nhận việc thanh toán của bạn thành công.'
												}
											</Typography>
										</Grid>
									</Grid>
								</Grid>
								{!matches && (
									<Grid item md={12} style={{ marginTop: '1.5rem' }}>
										<Button
											variant='contained'
											style={{ backgroundColor: '#3C78E9', color: '#fff' }}
											onClick={handlePaymentCourse}
										>
											Gửi đăng ký
										</Button>
									</Grid>
								)}
							</Grid>
						</Grid>
						<Grid
							item
							md={4}
							style={{
								paddingLeft: matches ? '0' : '2rem',
								marginTop: matches ? '1rem' : '0',
							}}
						>
							<Grid
								container
								style={{
									border: '1px solid #ccc',
									borderRadius: '5px',
									padding: '1rem',
								}}
							>
								<Grid
									container
									style={{
										borderBottom: '1px dashed #ccc',
										paddingBottom: '1rem',
									}}
								>
									<Grid item md={8}>
										<Typography>Đơn hàng</Typography>
									</Grid>
									<Grid item md={4} container justify='flex-end'>
										<Button
											startIcon={<SettingsIcon />}
											style={{ padding: '0' }}
											onClick={handleOpenCart}
										>
											Sửa
										</Button>
									</Grid>
								</Grid>
								<Grid
									container
									style={{
										borderBottom: '1px dashed #ccc',
										padding: '1rem 0 0.5rem 0',
									}}
								>
									{courses.map((course, index) => (
										<Grid
											key={index}
											container
											style={{ marginBottom: '0.5rem' }}
										>
											<Grid item md={8}>
												<Typography
													style={{ fontSize: '0.85rem', color: '#3C78E8' }}
												>
													{course.title}
												</Typography>
											</Grid>
											<Grid item md={4} container justify='flex-end'>
												<Typography
													style={{ fontSize: '0.85rem', fontWeight: 'bold' }}
												>
													{course.cost_new.toLocaleString('vi-VN', {
														style: 'currency',
														currency: 'VND',
													})}
												</Typography>
											</Grid>
										</Grid>
									))}
								</Grid>
								<Grid
									container
									style={{
										borderBottom: '1px dashed #ccc',
										padding: '1rem 0',
									}}
								>
									<Grid item md={8}>
										<Typography>Tạm tính</Typography>
									</Grid>
									<Grid item md={4} container justify='flex-end'>
										<Typography style={{ fontWeight: 'bold' }}>
											{total.toLocaleString('vi-VN', {
												style: 'currency',
												currency: 'VND',
											})}
										</Typography>
									</Grid>
								</Grid>
								<Grid
									container
									style={{
										padding: '1rem 0',
									}}
								>
									<Grid item md={8}>
										<Typography>Tổng cộng</Typography>
									</Grid>
									<Grid item md={4} container justify='flex-end'>
										<Typography
											style={{
												fontWeight: 'bold',
												color: '#FF0000',
												fontSize: '1.1rem',
											}}
										>
											{total.toLocaleString('vi-VN', {
												style: 'currency',
												currency: 'VND',
											})}
										</Typography>
									</Grid>
								</Grid>
							</Grid>
							{matches && (
								<Grid
									item
									md={12}
									style={{ marginTop: '1.5rem', marginBottom: '2rem' }}
									container
									justify='center'
								>
									<Button
										variant='contained'
										style={{ backgroundColor: '#3C78E9', color: '#fff' }}
										onClick={handlePaymentCourse}
									>
										Gửi đăng ký
									</Button>
								</Grid>
							)}
						</Grid>
					</Grid>
				</Grid>
			)}
		</MasterLayout>
	);
}

export default CourseCart;
