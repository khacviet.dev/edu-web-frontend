import { Grid, Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import classApi from 'api/classApi';
import courseApi from 'api/courseApi';
import userInfoApi from 'api/userInfoApi';
import SearchForm from 'common/components/SearchForm';
import MasterLayout from 'common/pages/MasterLayout';
import CourseCard from 'features/Course/components/CourseCard';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { getCookie } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

function CourseList(props) {
	let history = useHistory();
	const state = props.location.state;
	const notify = useContext(ToastifyContext);
	const [listCourse, setListCourse] = useState([]);
	const [listSubject, setListSubject] = useState([]);
	const [selectedSubject, setSelectedSubject] = useState('');
	const [isLoading, setIsLoading] = useState(false);
	const [buySort, setBuySort] = useState(false);

	const [value, setValue] = useState('female');
	const [page, setPage] = useState(1);
	const [isLast, setIsLast] = useState(false);
	const [isBuy, setIsBuy] = useState(() => {
		if (state && state.isMine) {
			return '1';
		}
		return '0';
	});
	const [isLogin, setIsLogin] = useState(false);

	const handleChange = (event) => {
		setValue(event.target.value);
	};

	const [filter, setFilter] = useState({
		subject_id: selectedSubject,
		is_buy: isBuy,
		q: '',
		limit: 6,
		page: page,
		is_change_filter: false,
	});

	const handleChangeSubject = (event) => {
		// if(resetFilter=false){
		// 	setResetFilter(true);
		// }
		setPage(1);
		setSelectedSubject(event.target.value);
		setFilter({
			...filter,
			subject_id: event.target.value,
			is_change_filter: true,
			page: 1,
		});
	};

	const handleChangeIsBuy = (event) => {
		setPage(1);
		setIsBuy(event.target.value);
		setFilter({
			...filter,
			is_buy: event.target.value,
			is_change_filter: true,
			page: 1,
		});
	};

	const handleDeleteFilter = () => {
		setPage(1);
		setSelectedSubject('');
		setFilter({
			...filter,
			subject_id: '',
			is_change_filter: true,
			page: 1,
			limit: 6,
		});
	};

	const handleSearchCourse = (e) => {
		setPage(1);
		setFilter({
			...filter,
			q: e.value,
			is_change_filter: true,
			page: 1,
		});
	};

	const handleBuyMost = () => {
		setPage(1);
		setSelectedSubject('');
		setFilter({
			subject_id: '',
			q: '',
			page: 1,
		});
		setBuySort((sort) => !sort);
	};

	//get subject filter
	useEffect(() => {
		setIsLoading((loading) => !loading);
		const getSubjectList = async () => {
			try {
				const response = await classApi.getSubjectList();
				const data = response.data;
				setListSubject(data);
				setIsLoading((loading) => !loading);
			} catch (error) {
				setIsLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		getSubjectList();
	}, []);

	useEffect(() => {
		const user = getCookie('c_user');
		if (user) {
			setIsLogin(true);
		}
	}, []);

	// useEffect(()=>{
	// 		const state = props.location.state;
	// 		if(state && state.isMine){
	// 			// setPage(1);
	// 			setIsBuy('1');
	// 			// setFilter({
	// 			// 	...filter,
	// 			// 	is_buy: '1',
	// 			// 	is_change_filter: true,
	// 			// 	page: 1,
	// 			// });
	// 		}
	// },[]);

	//get course list
	useEffect(() => {
		setIsLoading((loading) => !loading);
		const user_id = getCookie('c_user');
		if (user_id) {
			const getCourseList = async () => {
				try {
					const response = await userInfoApi.getCourseList(filter);
					const data = response.data;
					if (data.length == 0) {
						setIsLast(true);
					} else {
						setIsLast(false);
					}
					if (filter.is_change_filter == true) {
						setListCourse(data);
					} else {
						setListCourse((prev) => [...prev, ...data]);
					}

					// setListCourse(data);
					setIsLoading((loading) => !loading);
				} catch (error) {
					setIsLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getCourseList();
		} else {
			// khi chua login
			const getCourseList = async () => {
				try {
					const response = await courseApi.getCourseList(filter);
					const data = response.data;
					if (data.length == 0) {
						setIsLast(true);
					} else {
						setIsLast(false);
					}
					if (filter.is_change_filter == true) {
						setListCourse(data);
					} else {
						setListCourse((prev) => [...prev, ...data]);
					}

					// setListCourse(data);
					setIsLoading((loading) => !loading);
				} catch (error) {
					setIsLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getCourseList();
		}
	}, [filter]);

	// set page when scroll
	useEffect(() => {
		if (page > 1 && isLast == false) {
			setFilter({
				...filter,
				page: page,
				is_change_filter: false,
			});
		}
	}, [page, isLast]);

	const handleScroll = () => {
		const list = document.getElementById('listCourse');
		if (
			window.scrollY + window.innerHeight >=
			list.clientHeight + list.offsetTop
		) {
			setPage((a) => a + 1);
		}
	};

	useEffect(() => {
		window.addEventListener('scroll', handleScroll);
		return () => {
			window.removeEventListener('scroll', handleScroll);
		};
	}, []);

	const handleCourseRouterChange = () => {
		history.push('/course');
	};

	return (
		<MasterLayout>
			{isLoading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}

			<Grid container>
				<Grid item md={2}>
					<Grid container direction='column'>
						<Helmet>
							<title>Khóa học | Xoài Education</title>
						</Helmet>
						<Grid item md={12} style={{ marginBottom: '1rem' }}>
							{filter.subject_id != '' && (
								<Button
									variant='contained'
									color='secondary'
									onClick={handleDeleteFilter}
								>
									Xóa Lọc
								</Button>
							)}
						</Grid>
						{/* {isLogin && (
							<Grid>
								<Typography variant='h6' style={{ marginBottom: '0.5rem' }}>
									Trạng thái
								</Typography>
								<FormControl component='fieldset'>
									<RadioGroup
										name='filterIsBuy'
										value={isBuy}
										onChange={handleChangeIsBuy}
									>
										{filterIsBuy.map((filter, index) => (
											<FormControlLabel
												key={`filter${index}`}
												value={filter.value}
												control={<Radio />}
												label={`Lớp ${filter.text}`}
											/>
										))}
									</RadioGroup>
								</FormControl>
							</Grid>
						)} */}

						<Typography variant='h6' style={{ marginBottom: '0.5rem' }}>
							Môn học
						</Typography>
						<FormControl component='fieldset'>
							<RadioGroup
								name='subject'
								value={selectedSubject}
								onChange={handleChangeSubject}
							>
								{listSubject.map((subject, index) => (
									<FormControlLabel
										key={`subject${index}`}
										value={subject.subject_id}
										control={<Radio />}
										label={`Lớp ${subject.subject_name}`}
									/>
								))}
							</RadioGroup>
						</FormControl>
					</Grid>
				</Grid>
				<Grid id='listCourse' item md={10}>
					<Grid container spacing={3} alignItems='center'>
						<Grid item md={5}>
							<SearchForm
								onSubmit={handleSearchCourse}
								label='Tìm kiếm khóa học'
							/>
						</Grid>
						<Grid item md={7}>
							<Grid container justify='flex-end'>
								{/* <Button
									onClick={handleBuyMost}
									style={{ marginRight: '0.75rem' }}
									startIcon={<ShoppingCartIcon />}
									endIcon={ buySort ==false ?<ArrowDownwardIcon />:<ArrowUpwardIcon/>}
									variant='outlined'
								>
									Mua nhiều nhất
								</Button> */}
							</Grid>
						</Grid>
						<Grid container item md={12}></Grid>
						{listCourse.map((data, index) => (
							<Grid key={`course_${index}`} item md={4}>
								<CourseCard data={data}></CourseCard>
							</Grid>
						))}
					</Grid>
				</Grid>
			</Grid>
		</MasterLayout>
	);
}

export default CourseList;
