import { createSlice } from '@reduxjs/toolkit';
import { getCookie } from 'utils/common';

const getCartCookie = () => {
	const courses = getCookie('course_cart');
	if (courses === '') {
		return 0;
	}

	const courseSplit = courses.split('-');
	return courseSplit.length;
};

const courseSlice = createSlice({
	name: 'courses',
	initialState: getCartCookie(),
	reducers: {
		addToCart(state) {
			state = state + 1;
			return state;
		},
		deleteToCart(state) {
			state = state - 1;
			return state;
		},
		setDefault(state) {
			state = 0;
			return state;
		},
	},
});

const { reducer: courseReducer, actions } = courseSlice;
export const { addToCart, deleteToCart, setDefault } = actions;
export default courseReducer;
