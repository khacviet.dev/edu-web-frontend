import { Button, Grid, Typography } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import CallIcon from '@material-ui/icons/Call';
import classApi from 'api/classApi';
import RegisterForm from 'common/components/RegisterForm';
import ReportDialog from 'common/components/ReportDialog';
import React, { useContext, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import {
	checkExistCourseInCookie,
	getCookie,
	isLogin,
	setCookieCourseCart,
} from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';
import { addToCart } from '../courseSlice';
import './style.css';

const useStyles = makeStyles({
	textfield: {
		marginTop: '2rem',
	},
});

function CourseCard(props) {
	const history = useHistory();
	const notify = useContext(ToastifyContext);
	const classes = useStyles();
	const [isLoading, setIsLoading] = useState(false);
	const { data } = props;

	const current_price = BigInt(data.course_cost) - BigInt(data.discount);

	const handleRegis = (e) => {
		e.preventDefault();

		const request = {
			name: e.target.name.value,
			email: e.target.email.value,
			phone: e.target.phone.value,
			note: e.target.note.value,
			course_id: data.id,
			user_id: getCookie('c_user') || null,
		};
		const insertRegister = async () => {
			setIsLoading((loading) => !loading);
			try {
				const response = await classApi.insertRegister(request);

				setIsLoading((loading) => !loading);
				notify('success', response.message);
			} catch (error) {
				setIsLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		insertRegister();
	};

	const handleCourseDetailChange = (is_buy) => {
		if (is_buy) {
			history.push({
				pathname: `/coursedetail`,
				state: {
					course_id: data.id,
					isBuy: true,
				},
			});
		} else {
			history.push({
				pathname: `/coursedetail`,
				state: {
					course_id: data.id,
				},
			});
		}
	};

	const dispatch = useDispatch();

	const handleAddToCartClick = (courseId) => {
		if (!isLogin()) {
			notify('error', 'Vui lòng đăng nhập !');
			history.push('/signin');
			return;
		}
		if (checkExistCourseInCookie(courseId)) {
			notify('error', 'Khóa học đã tồn tại trong giỏ');
			return;
		}
		setCookieCourseCart(courseId);
		dispatch(addToCart());
		notify('success', 'Thêm vào giỏ thành công');
	};

	return (
		<Grid item md={12}>
			<Card className={classes.root}>
				<CardActionArea
					style={{ width: '100%', height: '24rem', position: 'relative' }}
				>
					{data.join_date == null && (
						<Grid container className='cardBuy'>
							<Grid container alignItems='center' jstify='center'>
								<Grid
									className='regis'
									item
									md={6}
									style={{ padding: '0 0.25rem' }}
								>
									<RegisterForm
										handleRegis={handleRegis}
										buttonText='Nhận tư vấn'
										buttonVariant='contained'
										icon={<CallIcon />}
									></RegisterForm>
								</Grid>
								<Grid item md={6} style={{ padding: '0 0.25rem' }}>
									<Button
										startIcon={<AddShoppingCartIcon />}
										variant='contained'
										color='secondary'
										onClick={() => handleAddToCartClick(data.id)}
									>
										Thêm vào giỏ
									</Button>
								</Grid>
							</Grid>

							<Grid item md={12} container justify='center'>
								<Button
									variant='contained'
									color='primary'
									onClick={() => handleCourseDetailChange(false)}
								>
									Xem chi tiết
								</Button>
							</Grid>
						</Grid>
					)}

					{data.join_date && (
						<Grid container className='cardBuy'>
							<Grid item md={6} style={{ padding: '1rem' }}>
								<Button
									variant='contained'
									color='primary'
									onClick={() => handleCourseDetailChange(true)}
								>
									Vào học bài
								</Button>
							</Grid>
							<Grid item md={6} style={{ padding: '1rem' }}>
								<ReportDialog
									courseId={data.id}
									className={data.title}
									icon={'1'}
								/>
							</Grid>
						</Grid>
					)}

					<div style={{ width: '100%', height: '17rem' }}>
						<img
							src={data.thumbnail}
							alt='Girl in a jacket'
							style={{ width: '100%', height: '17rem' }}
							loading='lazy'
						/>
					</div>

					<CardContent
						className='bref_info'
						style={{
							overflow: 'hidden',
							textOverflow: 'ellipsis',
							height: '10rem',
						}}
					>
						<Typography gutterBottom variant='h6'>
							{data.title}
						</Typography>
						<Typography variant='body2' color='textSecondary' component='p'>
							{data.brief_info}
						</Typography>
					</CardContent>
				</CardActionArea>

				<CardActions>
					<Grid container alignItems='center'>
						<Grid item md={6} style={{ paddingLeft: '0.5rem' }}>
							<Typography
								color='primary'
								variant='body1'
								style={{ fontSize: '1.25rem', color: 'red' }}
							>
								{new Intl.NumberFormat('vi-VN', {
									style: 'currency',
									currency: 'VND',
								}).format(current_price)}
							</Typography>
						</Grid>
						<Grid
							item
							md={6}
							style={{ paddingRight: '0.5rem', width: '50%' }}
							container
							justify='flex-end'
						>
							<Typography
								color='primary'
								variant='body1'
								style={{
									fontSize: '1.25rem',
									textDecorationLine: 'line-through',
								}}
							>
								{data.discount != '0' &&
									new Intl.NumberFormat('vi-VN', {
										style: 'currency',
										currency: 'VND',
									}).format(data.course_cost)}
							</Typography>
						</Grid>
					</Grid>
				</CardActions>
			</Card>
		</Grid>
	);
}

export default CourseCard;
