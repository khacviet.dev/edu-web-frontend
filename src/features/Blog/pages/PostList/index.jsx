import { Grid, Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import postApi from 'api/postApi';
import SearchForm from 'common/components/SearchForm';
import MasterLayout from 'common/pages/MasterLayout';
import PostCard from 'features/Blog/components/PostCard';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { ToastifyContext } from 'utils/ToastifyConfig';

function PostList(props) {
	const notify = useContext(ToastifyContext);
	const [listPost, setListPost] = useState([]);
	const [listCategory, setListCategory] = useState([]);
	const [selectedCategory, setSelectedCategory] = useState('');
	const [isLoading, setIsLoading] = useState(false);
	const [buySort, setBuySort] = useState(false);

	const [value, setValue] = useState('female');
	const [page, setPage] = useState(1);
	const [isLast, setIsLast] = useState(false);

	const handleChange = (event) => {
		setValue(event.target.value);
	};

	const state = props.location.state;

	const [filter, setFilter] = useState({
		category_id: selectedCategory,
		q: '',
		limit: 6,
		page: page,
		is_change_filter: false,
	});

	const handleChangeCategory = (event) => {
		// if(resetFilter=false){
		// 	setResetFilter(true);
		// }
		setPage(1);
		setSelectedCategory(event.target.value);
		setFilter({
			...filter,
			category_id: event.target.value,
			is_change_filter: true,
			page: 1,
		});
	};

	const handleDeleteFilter = () => {
		setPage(1);
		setSelectedCategory('');
		setFilter({
			...filter,
			category_id: '',
			is_change_filter: true,
			page: 1,
			limit: 6,
		});
	};

	const handleSearchPost = (e) => {
		setPage(1);
		setFilter({
			...filter,
			q: e.value,
			is_change_filter: true,
			page: 1,
		});
	};

	const handleBuyMost = () => {
		setPage(1);
		setSelectedCategory('');
		setFilter({
			category_id: '',
			q: '',
			page: 1,
		});
		setBuySort((sort) => !sort);
	};

	//get category filter
	useEffect(() => {
		setIsLoading((loading) => !loading);
		const getPostCategory = async () => {
			try {
				const response = await postApi.getPostCategory();
				const data = response.data;
				setListCategory(data);
				setIsLoading((loading) => !loading);
			} catch (error) {
				setIsLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		getPostCategory();
	}, []);

	//get post list
	useEffect(() => {
		setIsLoading((loading) => !loading);
		const getPosts = async () => {
			try {
				const response = await postApi.getPosts(filter);
				const data = response.data;
				if (data.length == 0) {
					setIsLast(true);
				} else {
					setIsLast(false);
				}

				if (filter.is_change_filter == true) {
					setListPost(data);
				} else {
					setListPost((prev) => [...prev, ...data]);
				}

				// setListCourse(data);
				setIsLoading((loading) => !loading);
			} catch (error) {
				setIsLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		getPosts();
	}, [filter]);

	// set page when scroll
	useEffect(() => {
		if (page > 1 && isLast == false) {
			setFilter({
				...filter,
				page: page,
				is_change_filter: false,
			});
		}
	}, [page, isLast]);

	const handleScroll = () => {
		const list = document.getElementById('listCourse');
		if (
			window.scrollY + window.innerHeight >=
			list.clientHeight + list.offsetTop
		) {
			setPage((a) => a + 1);
		}
	};

	useEffect(() => {
		window.addEventListener('scroll', handleScroll);
		return () => {
			window.removeEventListener('scroll', handleScroll);
		};
	}, []);

	useEffect(() => {
		if (state != undefined && state.category_id) {
			setSelectedCategory(state.category_id);
			setFilter({
				...filter,
				category_id: state.category_id,
				is_change_filter: true,
				page: 1,
				limit: 6,
			});
		}
	}, []);

	return (
		<MasterLayout>
			<Grid container>
				<Grid item md={2}>
					<Grid container direction='column'>
						{isLoading && (
							<div
								style={{
									position: 'fixed',
									top: '50%',
									left: '50%',
									width: '50px',
									transform: 'translate(-50%, -50%)',
									zIndex: '1000000000',
								}}
								className='cp-spinner cp-balls'
							></div>
						)}
						<Helmet>
							<title>Chia sẻ | Xoài Education</title>
						</Helmet>
						<Grid item md={12} style={{ marginBottom: '1rem' }}>
							{filter.category_id != '' && (
								<Button
									variant='contained'
									color='secondary'
									onClick={handleDeleteFilter}
								>
									Xóa Lọc
								</Button>
							)}
						</Grid>
						<Typography variant='h6' style={{ marginBottom: '0.5rem' }}>
							Thể loại
						</Typography>
						<FormControl component='fieldset'>
							<RadioGroup
								name='subject'
								value={selectedCategory}
								onChange={handleChangeCategory}
							>
								{listCategory.map((category, index) => (
									<FormControlLabel
										key={`subject${index}`}
										value={category.id}
										control={<Radio />}
										label={`${category.value}`}
									/>
								))}
							</RadioGroup>
						</FormControl>
					</Grid>
				</Grid>
				<Grid id='listCourse' item md={10}>
					<Grid container spacing={3} alignItems='center'>
						<Grid item sm={12} md={5} style={{ width: '100%' }}>
							<SearchForm
								onSubmit={handleSearchPost}
								label='Tìm kiếm bài viết'
							/>
						</Grid>
						<Grid item md={7}>
							<Grid container justify='flex-end'>
								{/* <Button
									onClick={handleBuyMost}
									style={{ marginRight: '0.75rem' }}
									startIcon={<ShoppingCartIcon />}
									endIcon={ buySort ==false ?<ArrowDownwardIcon />:<ArrowUpwardIcon/>}
									variant='outlined'
								>
									Mua nhiều nhất
								</Button> */}
							</Grid>
						</Grid>
						<Grid container item md={12}></Grid>
						{listPost.map((data, index) => (
							<Grid key={`course_${index}`} item md={4}>
								<PostCard data={data}></PostCard>
							</Grid>
						))}
					</Grid>
				</Grid>
			</Grid>
		</MasterLayout>
	);
}

export default PostList;
