import { Avatar, Divider, Grid, Paper, Typography } from '@material-ui/core';
import Fab from '@material-ui/core/Fab';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import classApi from 'api/classApi';
import postApi from 'api/postApi';
import MasterLayout from 'common/pages/MasterLayout';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link, useHistory, useParams } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import './style.css';

function PostDetail(props) {
	const notify = useContext(ToastifyContext);
	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.down('sm'));
	const history = useHistory();

	useEffect(() => {
		window.scrollTo({ top: 0 });
	}, []);
	const state = props.location.state;
	const [listLesson, setListLesson] = useState([]);
	const [post, setPost] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [currentCost, setCurrentCost] = useState(0);
	const [totalLesson, setTotalLesson] = useState(0);
	const [expanded, setExpanded] = React.useState('');
	const [listCategory, setListCategory] = useState([]);
	const [listCategoryId, setListCategoryId] = useState([]);
	const [dependList, setDependList] = useState([]);
	const [isData, setIsData] = useState(false);
	const refer = [
		{
			display: 'Bài viết',
			value: 'post',
		},
	];
	const handleChange = (panel) => (event, newExpanded) => {
		setExpanded(newExpanded ? panel : false);
	};
	// register
	const handleRegis = (e) => {
		e.preventDefault();
		const request = {
			name: e.target.name.value,
			email: e.target.email.value,
			phone: e.target.phone.value,
			note: e.target.note.value,
			course_id: course.id,
		};
		const insertRegister = async () => {
			setIsLoading((loading) => !loading);
			try {
				const response = await classApi.insertRegister(request);

				// const data = response.data;
				// setListSubject(data);
				// setListClass(data);

				// setTotalRow(response.data[0].count);
				setIsLoading((loading) => !loading);
				notify('success', response.message);
			} catch (error) {
				setIsLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		insertRegister();
	};

	// convert time to HH:MM:SS
	function convertHMS(value) {
		const sec = parseInt(value, 10); // convert value to number if it's string
		let hours = Math.floor(sec / 3600); // get hours
		let minutes = Math.floor((sec - hours * 3600) / 60); // get minutes
		let seconds = sec - hours * 3600 - minutes * 60; //  get seconds
		// add 0 if value < 10; Example: 2 => 02

		if (hours < 10) {
			hours = '0' + hours;
		}
		if (minutes < 10) {
			minutes = '0' + minutes;
		}
		if (seconds < 10) {
			seconds = '0' + seconds;
		}
		return (
			(hours != '00' ? hours + ':' : '') +
			(minutes != '00' ? minutes + ':' : '') +
			seconds
		); // Return is HH : MM : SS
	}

	// button start control
	const handleStartLearn = (source_id, index1, lesson_title, lesson_id) => {
		history.push({
			pathname: `/learn`,
			state: {
				source_id: source_id,
				course_title: course.title,
				course_id: course.id,
				current_source: index1 + 1,
				lesson_title: lesson_title,
				lesson_id: lesson_id,
			},
		});
	};
	const { postId } = useParams();
	// check state
	if (postId != undefined) {
		// get post
		useEffect(() => {
			const params = {
				post_id: postId,
			};
			setIsLoading((loading) => !loading);
			const getPostById = async () => {
				try {
					const response = await postApi.getPostById(params);
					const data = response.data;
					if (data.length == 0) {
						history.push({
							pathname: '/notfound',
						});
						setIsData(false);
					} else {
						setPost(data[0]);
						const catgory = data[0].category.split(',');
						const id = data[0].category_id.split(',');
						setListCategoryId(id);
						setListCategory(catgory);

						setIsData(true);
					}
					setIsLoading((loading) => !loading);
				} catch (error) {
					setIsLoading((loading) => !loading);
					notify('error', error.response.data.message);
				}
			};
			getPostById();
		}, []);

		// get post lien quan
		useEffect(() => {
			if (isData) {
				const params = {
					category_id: listCategoryId,
				};
				setIsLoading((loading) => !loading);
				const getPostDepend = async () => {
					try {
						const response = await postApi.getPostDepend(params);
						const data = response.data;
						setDependList(data);
						setIsLoading((loading) => !loading);
					} catch (error) {
						setIsLoading((loading) => !loading);
						notify('error', error.response.data.message);
					}
				};
				getPostDepend();
			}
		}, [listCategoryId, isData]);

		// update clcik

		useEffect(() => {
			if (isData) {
				const data = {
					post_id: postId,
				};
				setIsLoading((loading) => !loading);
				const updateClick = async () => {
					try {
						const response = await postApi.updateClick(data);
						setIsLoading((loading) => !loading);
					} catch (error) {
						setIsLoading((loading) => !loading);
						notify('error', error.response.data.message);
					}
				};
				updateClick();
			}
		}, [isData]);
	} else {
		// chuyển đến trang note found
	}

	const handleCategory = (category_id) => {
		history.push({
			pathname: `/post`,
			state: {
				category_id: category_id,
			},
		});
	};

	return (
		<MasterLayout
			breadcrumbs={matches ? false : true}
			referer={refer}
			header={
				post.title && post.title.length > 105
					? post.title.substr(0, 105) + '...'
					: post.title
			}
		>
			{isLoading == true && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			{post.title && (
				<Helmet>
					<title>{post.title} | Chia sẻ | Xoài Education</title>
				</Helmet>
			)}

			<Grid
				container
				spacing={3}
				style={{ marginTop: '1rem', marginBottom: ' 5rem' }}
			>
				<Grid item md={8}>
					<Paper>
						<Grid container md={12} style={{ padding: '1rem 1.5rem' }}>
							<Grid item sm={12} md={10}>
								<Typography style={{ fontSize: '1.5rem' }}>
									{post.title}
								</Typography>
							</Grid>
							<Grid
								style={{
									textAlign: 'end',
									marginTop: matches ? '0.5rem' : '0',
								}}
								item
								sm={12}
								md={2}
							>
								<Grid>{post.date}</Grid>
								<Grid>{post.number_click} lượt xem</Grid>
							</Grid>
						</Grid>
						<Grid container style={{ padding: '1rem 1.5rem' }}>
							{listCategory.length > 0 &&
								listCategory.map((category, index) => (
									<Fab
										onClick={() => handleCategory(listCategoryId[index])}
										key={`category${index}`}
										variant='extended'
										size='small'
										color='primary'
										aria-label='add'
									>
										{category}
									</Fab>
								))}
						</Grid>

						<Grid container>
							<Divider style={{ width: '100%' }} />
						</Grid>

						<Grid container style={{ padding: '1rem 1.5rem' }}>
							<div
								dangerouslySetInnerHTML={{ __html: post.full_content }}
							></div>
						</Grid>
					</Paper>
				</Grid>
				<Grid item md={4}>
					<Grid container>
						<Grid item md={12}>
							<Paper>
								<Grid container style={{ padding: '0.5rem 1rem' }}>
									<Typography
										variant='body1'
										style={{ fontSize: '1.25rem', marginBottom: '1rem' }}
									>
										Bài viết liên quan
									</Typography>
									{dependList.map((post, index) => (
										<Grid
											key={`depend${index}`}
											item
											md={12}
											style={{ marginBottom: '0.5rem' }}
										>
											<Link
												target='_parent'
												to={{
													pathname: `/postdetail/${post.post_id}`,
												}}
												style={{ fontSize: '1rem', textDecoration: 'none' }}
											>
												{post.title}
											</Link>

											<Divider style={{ width: '100%' }} />
										</Grid>
									))}
								</Grid>
							</Paper>
						</Grid>
						<Grid item md={12} style={{ marginTop: '2rem', width: '100%' }}>
							<Paper>
								<Grid container style={{ padding: '0.5rem 1rem' }}>
									<Typography variant='body1' style={{ fontSize: '1.25rem' }}>
										Tác giả
									</Typography>
								</Grid>
								<Grid container>
									<Divider style={{ width: '100%' }} />
								</Grid>
								<Grid container style={{ padding: '0.5rem 1rem 2rem 1rem' }}>
									<Grid
										container
										alignItems='center'
										direction='column'
										style={{ marginTop: '0.5rem' }}
									>
										<Avatar
											style={{ width: '5rem', height: 'auto' }}
											alt='a'
											src={post.avt_link}
										/>
										<Typography style={{ marginTop: '0.5rem' }} variant='body1'>
											{post.full_name}
										</Typography>
									</Grid>
									<Grid container justify='center'>
										<Typography style={{ marginTop: '1rem' }} variant='body2'>
											{post.short_description}
										</Typography>
									</Grid>
								</Grid>
							</Paper>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</MasterLayout>
	);
}

export default PostDetail;
