import { Divider, Grid, Typography } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';
import VisibilityIcon from '@material-ui/icons/Visibility';
import React from 'react';
import { useHistory } from 'react-router-dom';
import './style.css';

PostCard.propTypes = {};

const useStyles = makeStyles({
	textfield: {
		marginTop: '2rem',
	},
});

function PostCard(props) {
	const history = useHistory();
	const classes = useStyles();
	const { data } = props;

	const handlePostDetailChange = () => {
		history.push({
			pathname: `/postdetail/${data.post_id}`,
		});
	};
	return (
		<Grid item md={12}>
			<Card className={classes.root}>
				<CardActionArea
					style={{ width: '100%', height: '27rem' }}
					onClick={handlePostDetailChange}
				>
					<div style={{ width: '100%', height: '17rem' }}>
						<img
							src={data.thumbnail}
							alt='Girl in a jacket'
							style={{ width: '100%', height: '17rem' }}
							loading='lazy'
						/>
					</div>

					<CardContent
						className='bref_info'
						style={{
							overflow: 'hidden',
							textOverflow: 'ellipsis',
							height: '10rem',
						}}
					>
						<Typography gutterBottom variant='h6'>
							{data.title}
						</Typography>
						<Typography variant='body2' color='textSecondary' component='p'>
							{data.brief_info}
						</Typography>
					</CardContent>
				</CardActionArea>
				<Divider variant='middle' />
				<CardActions>
					<Grid container>
						<Grid container item md={6} style={{ paddingLeft: '0.5rem' }}>
							<Grid item md={6}>
								<VisibilityIcon></VisibilityIcon>
							</Grid>
							<Grid item md={6}>
								<Typography color='primary' variant='body1'>
									{data.number_click}
								</Typography>
							</Grid>
						</Grid>
						<Grid
							item
							md={6}
							container
							justify='flex-end'
							style={{ paddingRight: '0.5rem' }}
						>
							<Typography
								color='primary'
								variant='body1'
								style={{ fontSize: '1rem' }}
							>
								{data.date}
							</Typography>
						</Grid>
					</Grid>
				</CardActions>
			</Card>
		</Grid>
	);
}

export default PostCard;
