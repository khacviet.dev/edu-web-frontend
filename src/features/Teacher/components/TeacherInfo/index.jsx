import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

TeacherInfo.propTypes = {};

const useStyles = makeStyles({
	info: {
		paddingTop: '1rem',
	},
	avt: {
		width: ' 100%',
		height: 'auto',
		display: 'block',
		width: '128px !important',
		height: '128px !important',
		borderRadius: '50% !important',
	},
	full_name: {
		fontWeight: '700',
		fontSize: '1em !important',
		color: '#2074a1',
		textTransform: 'uppercase',
		transition: 'all .3s ease',
	},
});

function TeacherInfo(props) {
	const classes = useStyles();
	const { data } = props;
	const [description, setDescription] = useState([]);
	useEffect(() => {
		if (data && data.short_description != null) {
			const description = data.short_description.split('\n');
			setDescription(description);
		}
	}, [data]);

	return (
		<Grid container item md={12}>
			<Grid item md={2}>
				<Link
					to={`/teachers/${data.user_id}`}
					style={{ textDecoration: 'none' }}
				>
					<img
						className={classes.avt}
						src={data.avt_link}
						style={{ width: '100%', height: 'auto' }}
						loading='lazy'
					/>
				</Link>
			</Grid>
			<Grid item md={1}></Grid>
			<Grid item md={9}>
				<Grid className={classes.full_name} item md={12}>
					<Link
						to={`/teachers/${data.user_id}`}
						style={{ textDecoration: 'none' }}
					>
						{data.full_name}
					</Link>
				</Grid>
				<Grid item md={12}>
					{description.map((des) => (
						<Grid className={classes.info} item md={12}>
							{des}
						</Grid>
					))}
				</Grid>
			</Grid>
		</Grid>
	);
}

export default TeacherInfo;
