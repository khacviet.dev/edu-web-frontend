import { Grid, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import userApi from 'api/userApi';
import MasterLayout from 'common/pages/MasterLayout';
import TeacherInfo from 'features/Teacher/components/TeacherInfo';
import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';

TeacherList.propTypes = {};

const useStyles = makeStyles({
	root: {
		width: '100%',
	},
	paper: {
		padding: '1rem',
	},
	maginTop4: {
		marginTop: '4rem',
	},
});

function TeacherList(props) {
	const classes = useStyles();
	const [isLoading, setIsLoading] = useState(false);
	const [teachers, setTeachers] = useState([]);
	useEffect(() => {
		setIsLoading(true);
		const getTeachers = async () => {
			try {
				const response = await userApi.getTeachers();
				const data = response.data;
				setTeachers(data);
				setIsLoading(false);
			} catch (error) {
				setIsLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getTeachers();
	}, []);

	return (
		<MasterLayout>
			{isLoading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Giáo viên | Xoài Education</title>
			</Helmet>
			<Paper className={classes.paper}>
				<h1>Giảng viên</h1>
				<Grid container className={classes.maginTop4} spacing={4}>
					{teachers.map((teacher, index) => (
						<Grid
							item
							md={12}
							style={{ marginTop: '1rem', marginBottom: '1rem' }}
						>
							<TeacherInfo key={`teacher${index}`} data={teacher}></TeacherInfo>
						</Grid>
					))}
				</Grid>
			</Paper>
		</MasterLayout>
	);
}

export default TeacherList;
