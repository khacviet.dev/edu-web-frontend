import { Grid, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import courseApi from 'api/courseApi';
import userApi from 'api/userApi';
import MasterLayout from 'common/pages/MasterLayout';
import CourseCard from 'features/Course/components/CourseCard';
import TeacherInfo from 'features/Teacher/components/TeacherInfo';
import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router-dom';

TeacherDetail.propTypes = {};

const useStyles = makeStyles({
	root: {
		width: '100%',
	},
	paper: {
		padding: '1rem',
	},
	title: {
		fontSize: '1.5em',
		textAlign: 'center',
		color: '#464646',
		marginBottom: '1.5em',
		textTransform: 'uppercase',
		transition: 'all .3s ease',
		marginTop: '1rem',
	},
});

function TeacherDetail(props) {
	const classes = useStyles();
	const [isLoading, setIsLoading] = useState(false);
	const [teachers, setTeachers] = useState([]);
	const { userId } = useParams();
	const [page, setPage] = useState(1);
	const [listCourse, setListCourse] = useState([]);
	const [isLast, setIsLast] = useState(false);

	const [filter, setFilter] = useState({
		teacher_id: userId,
		limit: 6,
		page: page,
	});

	useEffect(() => {
		setIsLoading(true);
		const getTeacherById = async () => {
			try {
				const response = await userApi.getTeacherById(userId);
				const data = response.data;
				setTeachers(data);
				setIsLoading(false);
			} catch (error) {
				setIsLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		getTeacherById();
	}, []);

	useEffect(() => {
		setIsLoading(true);
		const getCourseListByTeacherId = async () => {
			try {
				const response = await courseApi.getCourseListByTeacherId(filter);
				const data = response.data;
				if (data.length == 0) {
					setIsLast(true);
				} else {
					setIsLast(false);
				}

				setListCourse((prev) => [...prev, ...data]);
				setIsLoading(false);
			} catch (error) {
				setIsLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getCourseListByTeacherId();
	}, [filter]);

	useEffect(() => {
		if (page > 1 && isLast == false) {
			setFilter({
				...filter,
				page: page,
			});
		}
	}, [page, isLast]);

	const handleScroll = () => {
		const list = document.getElementById('listCourse');
		if (
			window.scrollY + window.innerHeight >=
			list.clientHeight + list.offsetTop
		) {
			setPage((a) => a + 1);
		}
	};

	useEffect(() => {
		window.addEventListener('scroll', handleScroll);
		return () => {
			window.removeEventListener('scroll', handleScroll);
		};
	}, []);

	return (
		<MasterLayout>
			<Paper className={classes.paper}>
				{isLoading && (
					<div
						style={{
							position: 'fixed',
							top: '50%',
							left: '50%',
							width: '50px',
							transform: 'translate(-50%, -50%)',
							zIndex: '1000000000',
						}}
						className='cp-spinner cp-balls'
					></div>
				)}
				<Helmet>
					<title>Chi tiết | Giáo viên | Xoài Education</title>
				</Helmet>
				<Grid
					container
					direction='row'
					justify='center'
					alignItems='center'
					md={12}
				>
					<Grid item md={8}>
						{teachers.map((teacher, index) => (
							<TeacherInfo key={`teacher${index}`} data={teacher}></TeacherInfo>
						))}
					</Grid>
				</Grid>
			</Paper>
			<Grid
				id='listCourse'
				justify='center'
				alignItems='center'
				container
				md='12'
				className={classes.title}
			>
				<Grid item md={12}>
					Các khóa học của giảng viên
				</Grid>
				<Grid className={classes.paper} container item md={11} spacing={3}>
					{listCourse.map((data, index) => (
						<Grid key={`course_${index}`} item md={4}>
							<CourseCard data={data}></CourseCard>
						</Grid>
					))}
				</Grid>
			</Grid>
		</MasterLayout>
	);
}

export default TeacherDetail;
