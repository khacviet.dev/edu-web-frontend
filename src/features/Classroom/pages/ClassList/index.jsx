import { Button, Divider, Grid, Typography } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import classApi from 'api/classApi';
import MasterLayout from 'common/pages/MasterLayout';
import ClassModule from 'features/Classroom/components/ClassModule';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { ToastifyContext } from 'utils/ToastifyConfig';

const color = [
	{
		text_color: '#000000',
		background_1: '#F8F8FF',
		background_2: '#FFFFFF',
	},
	{
		text_color: '#FFFFFF',
		background_1: '#FF0000',
		background_2: '#CD0000',
	},
	{
		text_color: '#000000',
		background_1: '#F8F8FF',
		background_2: '#FFFFFF',
	},
	{
		text_color: '#FFFFFF',
		background_1: '#FFB90F',
		background_2: '#CD950C',
	},
	{
		text_color: '#FFFFFF',
		background_1: '#8DEEEE',
		background_2: '#79CDCD',
	},
	{
		text_color: '#FFFFFF',
		background_1: '#363636',
		background_2: '#FFFFFF',
	},
	{
		text_color: '#FFFFFF',
		background_1: '#FF7256',
		background_2: '#EE6A50',
	},
];

function ClassList(props) {
	const notify = useContext(ToastifyContext);

	// const [value, setValue] = useState('female');
	const [listClass, setListClass] = useState([]);
	const [listGrade, setListGrade] = useState([]);
	const [listSubject, setListSubject] = useState([]);

	const [selectedGrade, setSelectedGrade] = useState('');
	const [selectedSubject, setSelectedSubject] = useState('');
	// const [resetFilter, setResetFilter] = useState(false);
	const [isLast, setIsLast] = useState(false);
	const [isLoading, setIsLoading] = useState(false);
	const [page, setPage] = useState(1);
	// const error_messs = 'SomeThing Wrong';
	// background 1: o duoi background 2 la o tren ( Hien thi nam)
	const [filter, setFilter] = useState(
		{
			limit: 5,
			page: page,
			grade_level: selectedGrade,
			subject: selectedSubject,
			is_change_filter: false,
		},
		[]
	);

	const handleChangeGrade = (event) => {
		// if(resetFilter=false){
		// 	setResetFilter(true);
		// }
		setPage(1);
		setSelectedGrade(event.target.value);
		setFilter({
			...filter,
			grade_level: event.target.value,
			is_change_filter: true,
			page: 1,
		});
	};

	const handleChangeSubject = (event) => {
		// if(resetFilter=false){
		// 	setResetFilter(true);
		// }
		setPage(1);
		setSelectedSubject(event.target.value);
		setFilter({
			...filter,
			subject: event.target.value,
			is_change_filter: true,
			page: 1,
		});
	};

	const handleDeleteFilter = () => {
		setSelectedGrade('');
		setSelectedSubject('');
		setFilter({
			subject: '',
			grade_level: '',
			limit: 5,
			page: 1,
			is_change_filter: true,
		});
		setPage(1);
	};
	//get subject filter
	useEffect(() => {
		setIsLoading((loading) => !loading);
		const getSubjectList = async () => {
			try {
				const response = await classApi.getSubjectList();
				const data = response.data;
				setListSubject(data);
				// setListClass(data);
				setIsLoading((loading) => !loading);
				// setTotalRow(response.data[0].count);
				// setIsLoading(loading => !loading);
			} catch (error) {
				setIsLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		getSubjectList();
	}, []);

	// get gradelevel
	useEffect(() => {
		setIsLoading((loading) => !loading);
		const getGradeLevel = async () => {
			try {
				const response = await classApi.getGradeLevel();
				const data = response.data;
				setListGrade(data);
				setIsLoading((loading) => !loading);
			} catch (error) {
				setIsLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		getGradeLevel();
	}, []);

	// get list class
	useEffect(() => {
		setIsLoading((loading) => !loading);
		const getClasses = async () => {
			try {
				const response = await classApi.getClasses(filter);
				const data = response.data;
				if (data.length == 0) {
					setIsLast(true);
				} else {
					setIsLast(false);
				}

				if (filter.is_change_filter == true) {
					setListClass(data);
				} else {
					setListClass((prev) => [...prev, ...data]);
				}

				setIsLoading((loading) => !loading);
				// setTotalRow(response.data[0].count);
				// setIsLoading(loading => !loading);
			} catch (error) {
				setIsLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		getClasses();
	}, [filter]);

	// set page when scroll
	useEffect(() => {
		if (page > 1 && isLast == false) {
			setFilter({
				...filter,
				page: page,
				is_change_filter: false,
			});
		}
	}, [page, isLast]);
	// scroll
	const handleScroll = () => {
		const list = document.getElementById('list');
		if (
			window.scrollY + window.innerHeight >=
			list.clientHeight + list.offsetTop
		) {
			setPage((a) => a + 1);
		}
	};

	useEffect(() => {
		window.addEventListener('scroll', handleScroll);
		return () => {
			window.removeEventListener('scroll', handleScroll);
		};
	}, []);

	return (
		<MasterLayout>
			{isLoading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Lịch khai giảng | Xoài Education</title>
			</Helmet>
			<Grid container spacing={2}>
				<Grid item sm={12} md={2} style={{ width: '100%' }}>
					<Grid item md={12} style={{ marginBottom: '1rem' }}>
						{(filter.grade_level != '' || filter.subject != '') && (
							<Button
								variant='contained'
								color='secondary'
								onClick={handleDeleteFilter}
							>
								Xóa Lọc
							</Button>
						)}
					</Grid>

					<Grid item md={12}>
						<Typography variant='h6' style={{ marginBottom: '0.5rem' }}>
							Khối
						</Typography>
					</Grid>
					<Grid item md={12}>
						<FormControl component='fieldset'>
							<RadioGroup
								name='grade_level'
								value={selectedGrade}
								onChange={handleChangeGrade}
							>
								{listGrade.map((level, index) => (
									<FormControlLabel
										key={`grade_level${index}`}
										value={level.grade_level}
										control={<Radio />}
										label={`Lớp ${level.grade_level}`}
									/>
								))}
							</RadioGroup>
						</FormControl>
					</Grid>
					<Divider style={{ width: '70%', margin: '1.5rem 0' }} />
					<Grid item md={12}>
						<Typography variant='h6' style={{ marginBottom: '0.5rem' }}>
							Môn học
						</Typography>
					</Grid>
					<Grid item md={12}>
						<FormControl component='fieldset'>
							<RadioGroup
								name='subject'
								value={selectedSubject}
								onChange={handleChangeSubject}
							>
								{listSubject.map((subject, index) => (
									<FormControlLabel
										key={`subject${index}`}
										value={subject.subject_id}
										control={<Radio />}
										label={`Lớp ${subject.subject_name}`}
									/>
								))}
							</RadioGroup>
						</FormControl>
					</Grid>
				</Grid>
				<Grid id='list' item sm={12} md={8}>
					{listClass.map((data, index) => (
						<ClassModule
							key={`${data.class_id}${index}`}
							data={data}
							color={color}
						></ClassModule>
					))}
				</Grid>
			</Grid>
		</MasterLayout>
	);
}

export default ClassList;
