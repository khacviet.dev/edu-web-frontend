import { Button, Divider, Grid, Paper, Typography } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import AccessAlarmsTwoToneIcon from '@material-ui/icons/AccessAlarmsTwoTone';
import DateRangeIcon from '@material-ui/icons/DateRange';
import LocationOnTwoToneIcon from '@material-ui/icons/LocationOnTwoTone';
import classApi from 'api/classApi';
import RegisterForm from 'common/components/RegisterForm';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { getCookie } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

function ClassModule(props) {
	const history = useHistory();
	const notify = useContext(ToastifyContext);
	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.down('sm'));
	const [listSchedule, setListShcedule] = useState([]);
	const [chooseColor, setChooseColor] = useState({});
	const [isLoading, setIsLoading] = useState(false);
	const { data, color } = props;
	const newDate = data.start_day.split('-');

	const newDate_1 = new Date(data.start_day);
	const day = newDate_1.getDay();

	const list_date_and_time = data.day_of_week.split(',');
	const listSchedules = [];
	useEffect(() => {
		setChooseColor(color[day]);
		for (let i = 0; i < list_date_and_time.length; i++) {
			const date_and_time = list_date_and_time[i].split('-');
			const schedule = {
				day_of_week: parseInt(date_and_time[0]) + 1,
				start_time: date_and_time[1].trim().substring(0, 5),
				end_time: date_and_time[2].trim().substring(0, 5),
			};
			listSchedules.push(schedule);
		}
		setListShcedule(listSchedules);
	}, []);

	const handleRegis = (e) => {
		e.preventDefault();

		const request = {
			name: e.target.name.value,
			email: e.target.email.value,
			phone: e.target.phone.value,
			note: e.target.note.value,
			class_id: data.class_id,
			user_id: getCookie('c_user'),
		};
		const insertRegister = async () => {
			setIsLoading((loading) => !loading);
			try {
				const response = await classApi.insertRegister(request);

				// const data = response.data;
				// setListSubject(data);
				// setListClass(data);

				// setTotalRow(response.data[0].count);
				setIsLoading((loading) => !loading);
				notify('success', response.message);
			} catch (error) {
				setIsLoading((loading) => !loading);
				notify('error', error.response.data.message);
			}
		};
		insertRegister();
	};

	const handleGoTeacher = (teacher_id) => {
		history.push({
			pathname: `/teachers/${teacher_id}`,
		});
	};

	return (
		<Paper style={{ marginBottom: '2rem' }}>
			<Grid
				container
				style={{
					height: matches ? 'fit-content' : '12rem',
					boxShadow: '0 4px 4px rgb(0 0 0 / 15%)',
				}}
			>
				<Grid
					item
					container={matches ? false : true}
					style={{ height: '100%', width: '100%' }}
					sm={12}
					md={3}
				>
					<Grid
						item
						md={12}
						style={{
							backgroundColor: `${chooseColor.background_2}`,
							height: '16%',
							textAlign: 'center',
						}}
					>
						<Typography
							style={{
								marginTop: matches ? '0' : '0.5rem',
								color: `${chooseColor.text_color}`,
							}}
						>
							{newDate[0]}
						</Typography>
					</Grid>
					<Grid
						item
						md={12}
						style={{
							backgroundColor: `${chooseColor.background_1}`,
							height: '84%',
							textAlign: 'center',
						}}
					>
						<Typography
							style={{
								marginTop: matches ? '0' : '2rem',
								color: `${chooseColor.text_color}`,
								fontSize: '2.5rem',
							}}
						>
							{newDate[2]}
						</Typography>
						<Typography style={{ color: `${chooseColor.text_color}` }}>
							Tháng {newDate[1]}
						</Typography>
					</Grid>
				</Grid>

				<Grid
					item
					md={9}
					style={{
						paddingLeft: '1.2rem',
						paddingTop: '1.2rem',
						paddingLeft: '1.2rem',
					}}
				>
					<Grid container item md={12}>
						<Grid item md={6}>
							<Typography variant='h6'>{data.class_name}</Typography>
						</Grid>
					</Grid>
					<Grid item md={12}>
						<Typography
							style={{
								color: 'blue',
								fontSize: '0.85rem',
								cursor: 'pointer',
							}}
							onClick={() => handleGoTeacher(data.user_id)}
						>
							Giảng viên: {data.teacher_name}
						</Typography>
					</Grid>

					<Grid md={12} container>
						<Grid container item md={9}>
							{listSchedule.map((data, index) => (
								<Grid
									key={`${index}schedule`}
									container
									item
									md={6}
									style={{
										paddingTop: '0.5rem',
									}}
								>
									<Grid item md={5} style={{ color: '#666' }}>
										<Grid container alignItems='center'>
											<DateRangeIcon fontSize='small' />
											<Typography variant='body2'>
												&nbsp;
												{data.day_of_week == '1'
													? 'Chủ nhật'
													: `Thứ ${data.day_of_week}`}
											</Typography>
										</Grid>
									</Grid>
									<Grid item md={6} style={{ color: '#666' }}>
										<Grid container alignItems='center'>
											<AccessAlarmsTwoToneIcon fontSize='small' />
											<Typography variant='body2'>
												&nbsp;{`${data.start_time} - ${data.end_time}`}
											</Typography>
										</Grid>
									</Grid>
								</Grid>
							))}
						</Grid>

						<Grid item md={3} style={{ color: '#666', paddingTop: '0.5rem' }}>
							<Grid container alignItems='center'>
								<LocationOnTwoToneIcon fontSize='small' />
								<Typography variant='body2'>&nbsp;{data.address}</Typography>
							</Grid>
						</Grid>
					</Grid>

					<Divider style={{ width: '100%', marginTop: '0.3rem' }} />

					<Grid container item md={12}>
						<Grid item md={3}>
							{/* <Button
                    size='medium'
                    style={{  color: 'green' }}
                >
                    Đăng ký
                </Button> */}
							{/* <Button   size='medium'
                            style={{  color: 'green' }} 
                            onClick={handleClickOpen}>
                        Đăng ký
                    </Button>
                    <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    
                        <DialogTitle id="form-dialog-title">Tư vấn</DialogTitle>
                        <form  onSubmit={(e)=>handleRegis(e)}  >
                        <DialogContent>
                        <DialogContentText>
                            Vui lòng điền và gửi thông tin cá nhân để được tư vấn miễn phí về các lớp học
                            {isLoading == true && (
					<div
                    style={{
                        position: 'fixed',
                        top: '50%',
                        left: '50%',
                        width: '50px',
                        transform: 'translate(-50%, -50%)',
                        zIndex: '1000000000',
                    }}
                    className='cp-spinner cp-balls'
                ></div>
				)}
                        </DialogContentText>
                       
                        <TextField
                            className={classes.textfield}
                            required
                            variant='outlined'
                            autoFocus
                            margin="dense"
                            name='name'
                            id="name"
                            label="Họ và tên"
                            fullWidth
                        />
                         <TextField
                            className={classes.textfield}
                            required
                            variant='outlined'
                            autoFocus
                            margin="dense"
                            name='email'
                            id="email"
                            label="Email Address"
                            type="email"
                            fullWidth
                        />
                         <TextField
                            className={classes.textfield}
                            required
                            variant='outlined'
                            autoFocus
                            margin="dense"
                            name='phone'
                            id="phone"
                            label="Số điện thoại"
                            inputProps={{  pattern: '^[0-9.]*' }}
                            fullWidth
                        />
                         <TextField
                            className={classes.textfield}
                            autoFocus
                            variant='outlined'
                            margin="dense"
                            name='note'
                            id="note"
                            label="Ghi chú"
                            multiline
                            rows={4}
                           
                            fullWidth
                        />
                        </DialogContent>
                        <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button type='submit' color="primary">
                            Subscribe
                        </Button>
                    
                        </DialogActions>
                        </form>
                    </Dialog> */}

							<RegisterForm
								handleRegis={handleRegis}
								buttonText='Đăng ký'
								buttonSize='medium'
								textColor='green'
							></RegisterForm>
						</Grid>
						<Grid item md={8}>
							<Button
								size='medium'
								style={{ color: 'red', float: 'right' }}
								disabled
							>
								{data.note}
							</Button>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</Paper>
	);
}

export default ClassModule;
