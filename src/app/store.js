import { configureStore } from '@reduxjs/toolkit';
import courseReducer from 'features/Course/courseSlice';
import avatarReducer from 'features/User/avatarSlice';

const rootReducer = {
	courses: courseReducer,
	avatars: avatarReducer,
};

const store = configureStore({
	reducer: rootReducer,
});

export default store;
