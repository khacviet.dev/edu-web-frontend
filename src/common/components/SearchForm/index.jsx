import { InputAdornment, TextField } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';

SearchForm.propTypes = {
	onSubmit: PropTypes.func.isRequired,
	label: PropTypes.string,
};

SearchForm.defaultProps = {
	label: '',
};

export default function SearchForm(props) {
	const { onSubmit, label } = props;
	const [search, setSearch] = useState('');
	const typingRef = useRef(null);

	const handleSearchChange = (e) => {
		const value = e.target.value;
		setSearch(value);

		if (!onSubmit) return;

		if (typingRef.current) {
			clearTimeout(typingRef.current);
		}

		typingRef.current = setTimeout(() => {
			const formValues = {
				value,
			};

			onSubmit(formValues);
		}, 400);
	};

	return (
		<TextField
			fullWidth
			variant='outlined'
			size='small'
			label={label}
			value={search}
			onChange={handleSearchChange}
			InputProps={{
				endAdornment: (
					<InputAdornment position='start'>
						<SearchIcon color='action' />
					</InputAdornment>
				),
			}}
		/>
	);
}
