import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import './MySilde.css';

function MySlide(props) {
	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.down('sm'));
	const settings = {
		dots: matches ? false : true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		arrows: false,
		autoplaySpeed: 6000,
	};

	const handleSlide = (redirect_link) => {
		window.open(redirect_link);
	};

	return (
		<Slider style={{ width: matches ? '91vw' : '100%' }} {...settings}>
			{props.data.map((slide, index) => (
				<div
					key={`slide${index}`}
					onClick={() => handleSlide(slide.redirect_link)}
					style={{ cursor: 'pointer' }}
				>
					<img
						style={{ height: 'auto', cursor: 'pointer' }}
						src={slide.image_link}
						alt='a'
						loading='lazy'
					/>
				</div>
			))}
		</Slider>
	);
}

export default MySlide;
