import {
	Box,
	Button,
	Divider,
	Grid,
	IconButton,
	makeStyles,
	Modal,
	Paper,
	Typography,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import CloseIcon from '@material-ui/icons/Close';
import { Form, Formik } from 'formik';
import React, { useContext, useRef, useState } from 'react';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles((theme) => ({
	modal: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%,-50%)',
		padding: theme.spacing(1.5),
		width: 500,
		outline: 0,
	},
	createButton: {
		borderRadius: '20px',
	},
	createPostTitle: {
		marginLeft: '57px',
		fontSize: '1.25rem',
		fontWeight: 'bold',
	},
	userName: {
		marginLeft: '10px',
	},
	browserImageButton: {
		cursor: 'pointer',
	},
	image: {
		width: '100%',
		height: 'auto',
	},
	closeImageButton: {
		position: 'absolute',
		top: '10px',
		right: '10px',
		backgroundColor: '#fff',
	},
	postPanel: {
		marginTop: '1rem',
	},
}));

function ChangeAvatarModal(props) {
	const notify = useContext(ToastifyContext);
	const { handleChangeAvatar } = props;
	const classes = useStyles();

	const [open, setOpen] = React.useState(false);

	const handleOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const [image, setImage] = useState('');

	const handleImageUpload = (e) => {
		if (
			e.target.files[0].type !== 'image/jpeg' &&
			e.target.files[0].type !== 'image/jpg' &&
			e.target.files[0].type !== 'image/png'
		) {
			notify('error', 'File tải lên không đúng định dạng ảnh');
			imageREF.current.value = '';
			setImage('');
			return;
		}

		const file = e.target.files[0];
		const reader = new FileReader();

		reader.onload = (c) => {
			setImage(c.target.result);
		};

		reader.readAsDataURL(file);
	};

	const handleRemoveImageUpload = () => {
		imageREF.current.value = '';
		setImage('');
	};

	const imageREF = useRef(null);

	const initialValues = {
		image: null,
	};

	return (
		<Formik
			initialValues={initialValues}
			onSubmit={(values, { resetForm }) => {
				let formData = new FormData();
				if (values.image !== null) {
					formData.append('image', values.image);
				}

				if (handleChangeAvatar) {
					handleChangeAvatar(formData);
					resetForm({
						image: null,
					});
					handleRemoveImageUpload();
				}

				handleClose();
			}}
		>
			{(formikProps) => {
				return (
					<Box>
						<CameraAltIcon
							onClick={handleOpen}
							style={{ color: '#000', cursor: 'pointer' }}
						/>

						<Modal open={open} onClose={handleClose}>
							{
								<Paper className={classes.modal}>
									<Form encType='multipart/form-data'>
										<Grid container alignItems='center' spacing={1}>
											<Grid
												item
												xs={10}
												md={10}
												lg={10}
												container
												justify='center'
											>
												<Box className={classes.createPostTitle}>
													Thay đổi ảnh đại diện
												</Box>
											</Grid>
											<Grid
												item
												xs={2}
												md={2}
												lg={2}
												container
												justify='flex-end'
											>
												<IconButton onClick={handleClose}>
													<CloseIcon />
												</IconButton>
											</Grid>
											<Grid item xs={12} md={12} lg={12}>
												<Divider />
											</Grid>
											<Grid
												item
												xs={12}
												md={12}
												lg={12}
												container
												style={{ marginTop: '0.5rem', marginBottom: '0.5rem' }}
											>
												<label
													className={classes.browserImageButton}
													htmlFor='image'
												>
													<Grid container alignItems='center'>
														<AddIcon color='primary' fontSize='large' />
														<Typography variant='body1'>
															&nbsp;Chọn ảnh
														</Typography>
													</Grid>
												</label>
												<input
													ref={imageREF}
													id='image'
													style={{ display: 'none' }}
													type='file'
													onChange={(e) => {
														handleImageUpload(e);
														formikProps.setFieldValue(
															'image',
															e.currentTarget.files[0]
														);
													}}
												/>
											</Grid>
											{image !== '' && (
												<Grid
													item
													xs={12}
													md={12}
													lg={12}
													style={{
														position: 'relative',
													}}
												>
													<img
														style={{
															width: '100%',
															height: 'auto',
															maxHeight: '267.75px',
															objectFit: 'cover',
														}}
														className={classes.image}
														src={image}
														alt='aaa'
														loading='lazy'
													/>
													<IconButton
														onClick={() => {
															formikProps.setFieldValue('image', null);
															imageREF.current.value = '';
															setImage('');
														}}
														size='small'
														className={classes.closeImageButton}
													>
														<CloseIcon />
													</IconButton>
												</Grid>
											)}
											{image && (
												<Grid
													item
													xs={12}
													md={12}
													lg={12}
													className={classes.postPanel}
												>
													<Grid container justify='flex-end'>
														<Button
															variant='contained'
															color='secondary'
															style={{ marginRight: '0.5rem' }}
														>
															Hủy
														</Button>
														<Button
															type='submit'
															variant='contained'
															color='primary'
														>
															Lưu
														</Button>
													</Grid>
												</Grid>
											)}
										</Grid>
									</Form>
								</Paper>
							}
						</Modal>
					</Box>
				);
			}}
		</Formik>
	);
}

export default ChangeAvatarModal;
