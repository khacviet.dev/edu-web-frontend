import {
	AppBar,
	Avatar,
	Badge,
	Box,
	Divider,
	Drawer,
	Grid,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Menu,
	MenuItem,
	Toolbar,
	Tooltip,
	Typography,
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import HomeIcon from '@material-ui/icons/Home';
import LocalGroceryStoreIcon from '@material-ui/icons/LocalGroceryStore';
import MenuIcon from '@material-ui/icons/Menu';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import PollIcon from '@material-ui/icons/Poll';
import ScheduleIcon from '@material-ui/icons/Schedule';
import SchoolIcon from '@material-ui/icons/School';
import SettingsIcon from '@material-ui/icons/Settings';
import StarIcon from '@material-ui/icons/Star';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import authApi from 'api/authApi';
import xoai from 'assets/logos/xoai.png';
import { setDefault } from 'features/Course/courseSlice';
import { getAvatar } from 'features/User/avatarSlice';
import React, { useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { deleteAllCookieCart, isLogin } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

const drawerWidth = '100vw';

const useStyles = makeStyles((theme) => ({
	title: {
		flexGrow: 1,
		cursor: 'pointer',
	},
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
		color: '#111',
		backgroundColor: '#fff',
	},
	drawerPaper: {
		width: drawerWidth,
		background: theme.palette.primary.main,
		color: '#111',
		backgroundColor: '#fff',
	},
	drawerHeader: {
		padding: theme.spacing(0, 1),
		...theme.mixins.toolbar,
	},
	nested: {
		paddingLeft: theme.spacing(4),
	},
}));

function MyNavBar(props) {
	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.down('sm'));
	const courseCart = useSelector((state) => state.courses);
	const notify = useContext(ToastifyContext);
	const classes = useStyles();
	const avatars = useSelector((state) => state.avatars);
	const dispatch = useDispatch();
	let history = useHistory();

	const [open, setOpen] = useState(false);

	const [anchorEl, setAnchorEl] = useState(null);

	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleCloseMenu = () => {
		setAnchorEl(null);
	};

	const handleRouteChange = (params, mineCourse) => {
		if (mineCourse) {
			history.push({
				pathname: `/mycourse`,
				state: {
					isMine: true,
				},
			});
		} else {
			history.push(`/${params}`);
		}
	};

	const handleDrawerClose = () => {
		setOpen(false);
	};

	const handleDrawerOpen = () => {
		setOpen(true);
	};

	const logout = () => {
		const post = async () => {
			try {
				await authApi.logout();
				deleteAllCookieCart();
				dispatch(setDefault());
				history.push('/');
			} catch (error) {
				notify('error', error.response.data.message);
			}
		};
		post();
	};

	const handleOpenAdmin = () => {
		window.open('https://xoaicms.site');
	};

	const routeChange = (path) => {
		history.push(path);
		setOpen(false);
	};

	useEffect(() => {
		if (avatars.avtLink === '') {
			dispatch(getAvatar());
		}
	}, []);

	return (
		<AppBar position='static' color='primary'>
			<Drawer
				className={classes.drawer}
				variant='persistent'
				anchor='left'
				open={open}
				classes={{
					paper: classes.drawerPaper,
				}}
			>
				<Grid container style={{ width: '100%' }}>
					<Grid
						item
						md={8}
						style={{ width: '75%', paddingLeft: '0.5rem' }}
						container
						alignItems='center'
					>
						<img
							src={xoai}
							style={{
								width: '40px',
								height: '40px',
								cursor: 'pointer',
								marginRight: '0.5rem',
							}}
							alt='none'
							onClick={() => routeChange('/')}
							loading='lazy'
						/>
						<Typography
							style={{ fontWeight: 'bold' }}
							onClick={() => routeChange('/')}
						>
							Xoài Education
						</Typography>
					</Grid>
					<Grid
						item
						md={4}
						style={{ width: '25%' }}
						container
						justify='flex-end'
					>
						<IconButton onClick={handleDrawerClose}>
							<ChevronLeftIcon style={{ color: '#111' }} />
						</IconButton>
					</Grid>
				</Grid>
				<Divider />
				<List>
					<ListItem button onClick={() => routeChange('/')}>
						<ListItemIcon>
							<HomeIcon />
						</ListItemIcon>
						<ListItemText primary={'Trang chủ'} />
					</ListItem>
					<ListItem button onClick={() => routeChange('/course')}>
						<ListItemIcon>
							<SchoolIcon />
						</ListItemIcon>
						<ListItemText primary={'Khóa học'} />
					</ListItem>
					<ListItem button onClick={() => routeChange('/class')}>
						<ListItemIcon>
							<ScheduleIcon />
						</ListItemIcon>
						<ListItemText primary={'Lịch khai giảng'} />
					</ListItem>
					<ListItem button onClick={() => routeChange('/post')}>
						<ListItemIcon>
							<PollIcon />
						</ListItemIcon>
						<ListItemText primary={'Chia sẻ'} />
					</ListItem>
					<ListItem button onClick={() => routeChange('/teachers')}>
						<ListItemIcon>
							<PeopleAltIcon />
						</ListItemIcon>
						<ListItemText primary={'Giảng viên'} />
					</ListItem>
					{isLogin() && (
						<ListItem
							button
							onClick={() => handleRouteChange('mycourse', true)}
						>
							<ListItemIcon>
								<StarIcon />
							</ListItemIcon>
							<ListItemText primary={'Khóa học của tôi'} />
						</ListItem>
					)}
					{!isLogin() && (
						<ListItem button onClick={() => routeChange('/signup')}>
							<ListItemIcon>
								<AddCircleOutlineIcon />
							</ListItemIcon>
							<ListItemText primary={'Đăng ký'} />
						</ListItem>
					)}
					{!isLogin() && (
						<ListItem button onClick={() => routeChange('/signin')}>
							<ListItemIcon>
								<VpnKeyIcon />
							</ListItemIcon>
							<ListItemText primary={'Đăng nhập'} />
						</ListItem>
					)}
				</List>
			</Drawer>
			{!matches ? (
				<Toolbar>
					<img
						src={xoai}
						style={{
							width: '50px',
							height: '50px',
							marginBottom: '0.25rem',
							cursor: 'pointer',
						}}
						alt='none'
						onClick={() => handleRouteChange('')}
						loading='lazy'
					/>
					<Typography
						variant='h6'
						className={classes.title}
						onClick={() => handleRouteChange('')}
					>
						&nbsp;&nbsp;Xoài Education
					</Typography>
					<Button color='inherit' onClick={() => handleRouteChange('course')}>
						Khóa học&nbsp;&nbsp;&nbsp;&nbsp;
					</Button>
					<Button color='inherit' onClick={() => handleRouteChange('class')}>
						Lịch khai giảng&nbsp;&nbsp;&nbsp;&nbsp;
					</Button>
					<Button color='inherit' onClick={() => handleRouteChange('post')}>
						Chia sẻ&nbsp;&nbsp;&nbsp;&nbsp;
					</Button>
					<Button color='inherit' onClick={() => handleRouteChange('teachers')}>
						Giảng viên
					</Button>
					{isLogin() && (
						<Button
							color='inherit'
							onClick={() => handleRouteChange('mycourse', true)}
						>
							Khóa học của tôi
						</Button>
					)}
					{isLogin() && (
						<Tooltip title='Giỏ hàng'>
							<Badge
								badgeContent={courseCart}
								color='secondary'
								style={{ margin: '0 1rem 0 1.5rem', cursor: 'pointer' }}
								onClick={() => handleRouteChange('course-cart')}
							>
								<LocalGroceryStoreIcon style={{ fontSize: '1.75rem' }} />
							</Badge>
						</Tooltip>
					)}
					{isLogin() ? (
						<div>
							<Avatar
								src={avatars.avtLink}
								style={{ cursor: 'pointer', marginLeft: '0.5rem' }}
								onClick={handleClick}
							></Avatar>
							<Menu
								anchorEl={anchorEl}
								keepMounted
								open={Boolean(anchorEl)}
								onClose={handleCloseMenu}
							>
								<Box onClick={() => handleRouteChange('profile')}>
									<MenuItem onClick={handleCloseMenu}>
										<ListItemIcon>
											<PersonOutlineIcon fontSize='small' />
										</ListItemIcon>
										<ListItemText primary='Thông tin cá nhân' />
									</MenuItem>
								</Box>
								<Box onClick={() => handleRouteChange('changepassword')}>
									<MenuItem onClick={handleCloseMenu}>
										<ListItemIcon>
											<VpnKeyIcon fontSize='small' />
										</ListItemIcon>
										<ListItemText primary='Đổi mật khẩu' />
									</MenuItem>
								</Box>

								<Box onClick={handleOpenAdmin}>
									<MenuItem onClick={handleCloseMenu}>
										<ListItemIcon>
											<SettingsIcon fontSize='small' />
										</ListItemIcon>
										<ListItemText primary='Xoài CMS' />
									</MenuItem>
								</Box>

								<Box onClick={logout}>
									<MenuItem onClick={handleCloseMenu}>
										<ListItemIcon>
											<ExitToAppIcon fontSize='small' />
										</ListItemIcon>
										<ListItemText primary='Đăng xuất' />
									</MenuItem>
								</Box>
							</Menu>
						</div>
					) : (
						<div>
							<Button
								color='inherit'
								onClick={() => handleRouteChange('signup')}
							>
								Đăng ký
							</Button>
							<Button
								color='inherit'
								onClick={() => handleRouteChange('signin')}
							>
								Đăng nhập
							</Button>
						</div>
					)}
				</Toolbar>
			) : (
				<Toolbar>
					<Grid container alignItems='center'>
						<Grid item md={6} style={{ width: '50%' }}>
							<IconButton
								edge='start'
								color='inherit'
								aria-label='menu'
								onClick={handleDrawerOpen}
							>
								<MenuIcon />
							</IconButton>
						</Grid>
						<Grid item md={6} style={{ width: '50%' }}>
							{isLogin() && (
								<Grid container justify='flex-end' alignItems='center'>
									<Tooltip title='Giỏ hàng'>
										<Badge
											badgeContent={courseCart}
											color='secondary'
											style={{ marginRight: '0.5rem', cursor: 'pointer' }}
											onClick={() => handleRouteChange('course-cart')}
										>
											<LocalGroceryStoreIcon style={{ fontSize: '1.75rem' }} />
										</Badge>
									</Tooltip>
									<Avatar
										src={avatars.avtLink}
										style={{ cursor: 'pointer', marginLeft: '0.5rem' }}
										onClick={handleClick}
									></Avatar>
									<Menu
										anchorEl={anchorEl}
										keepMounted
										open={Boolean(anchorEl)}
										onClose={handleCloseMenu}
									>
										<Box onClick={() => handleRouteChange('profile')}>
											<MenuItem onClick={handleCloseMenu}>
												<ListItemIcon>
													<PersonOutlineIcon fontSize='small' />
												</ListItemIcon>
												<ListItemText primary='Thông tin cá nhân' />
											</MenuItem>
										</Box>
										<Box onClick={() => handleRouteChange('changepassword')}>
											<MenuItem onClick={handleCloseMenu}>
												<ListItemIcon>
													<VpnKeyIcon fontSize='small' />
												</ListItemIcon>
												<ListItemText primary='Đổi mật khẩu' />
											</MenuItem>
										</Box>
										<Box onClick={handleOpenAdmin}>
											<MenuItem onClick={handleCloseMenu}>
												<ListItemIcon>
													<SettingsIcon fontSize='small' />
												</ListItemIcon>
												<ListItemText primary='Xoài CMS' />
											</MenuItem>
										</Box>
										<Box onClick={logout}>
											<MenuItem onClick={handleCloseMenu}>
												<ListItemIcon>
													<ExitToAppIcon fontSize='small' />
												</ListItemIcon>
												<ListItemText primary='Đăng xuất' />
											</MenuItem>
										</Box>
									</Menu>
								</Grid>
							)}
						</Grid>
					</Grid>
				</Toolbar>
			)}
		</AppBar>
	);
}

export default MyNavBar;
