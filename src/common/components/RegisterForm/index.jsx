import { Box, Button, Grid } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import CloseIcon from '@material-ui/icons/Close';
import userInfoApi from 'api/userInfoApi';
import React, { useContext } from 'react';
import { getCookie } from 'utils/common';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles({
	textfield: {
		marginTop: '2rem',
	},
});

export default function RegisterForm(props) {
	const notify = useContext(ToastifyContext);
	const [open, setOpen] = React.useState(false);
	const classes = useStyles();
	const { buttonVariant, buttonColor, buttonSize, textColor, icon } = props;
	const userId = getCookie('c_user');
	const [user, setUser] = React.useState({});
	const [isLoading, setIsLoading] = React.useState(false);

	const handleClickOpen = () => {
		if (userId) {
			const getUserInfo = async () => {
				try {
					const response = await userInfoApi.getUserInfo(userId);
					const data = response.data;
					setUser(data[0]);
					setIsLoading(true);
				} catch (error) {
					notify('error', error.response.data.message);
				}
			};
			getUserInfo();
		}
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleRegisD = (e) => {
		props.handleRegis(e);
		setOpen(false);
	};

	return (
		<Grid item md={12}>
			<Button
				size={buttonSize}
				style={{ color: textColor }}
				variant={buttonVariant}
				color={buttonColor}
				onClick={handleClickOpen}
				startIcon={icon}
			>
				{props.buttonText}
			</Button>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby='form-dialog-title'
			>
				<DialogTitle id='form-dialog-title'>
					<Box display='flex' alignItems='center'>
						<Box flexGrow={1}> Đăng ký</Box>
						<Box>
							<IconButton onClick={handleClose}>
								<CloseIcon />
							</IconButton>
						</Box>
					</Box>
				</DialogTitle>
				<form onSubmit={(e) => handleRegisD(e)}>
					{isLoading == true && (
						<DialogContent>
							<DialogContentText>
								Vui lòng điền và gửi thông tin cá nhân để được tư vấn miễn phí
								về các lớp học
							</DialogContentText>

							<TextField
								className={classes.textfield}
								defaultValue={user.full_name}
								required
								variant='outlined'
								autoFocus
								margin='dense'
								name='name'
								id='name'
								label='Họ và tên'
								fullWidth
							/>
							<TextField
								className={classes.textfield}
								defaultValue={user.email}
								required
								variant='outlined'
								margin='dense'
								name='email'
								id='email'
								label='Email Address'
								type='email'
								fullWidth
							/>
							<TextField
								className={classes.textfield}
								required
								defaultValue={user.phone}
								variant='outlined'
								margin='dense'
								name='phone'
								id='phone'
								label='Số điện thoại'
								inputProps={{ pattern: '^[0-9.]*' }}
								fullWidth
							/>
							<TextField
								className={classes.textfield}
								variant='outlined'
								margin='dense'
								name='note'
								id='note'
								label='Ghi chú'
								multiline
								rows={4}
								fullWidth
							/>
						</DialogContent>
					)}

					{isLoading == false && (
						<DialogContent>
							<DialogContentText>
								Vui lòng điền và gửi thông tin cá nhân để được tư vấn miễn phí
								về các lớp học
							</DialogContentText>

							<TextField
								className={classes.textfield}
								defaultValue={user.full_name}
								required
								variant='outlined'
								autoFocus
								margin='dense'
								name='name'
								id='name'
								label='Họ và tên'
								fullWidth
							/>
							<TextField
								className={classes.textfield}
								defaultValue={user.email}
								required
								variant='outlined'
								margin='dense'
								name='email'
								id='email'
								label='Email Address'
								type='email'
								fullWidth
							/>
							<TextField
								className={classes.textfield}
								required
								defaultValue={user.phone}
								variant='outlined'
								margin='dense'
								name='phone'
								id='phone'
								label='Số điện thoại'
								inputProps={{ pattern: '^[0-9.]*' }}
								fullWidth
							/>
							<TextField
								className={classes.textfield}
								variant='outlined'
								margin='dense'
								name='note'
								id='note'
								label='Ghi chú'
								multiline
								rows={4}
								fullWidth
							/>
						</DialogContent>
					)}

					<DialogActions>
						<Button onClick={handleClose} color='primary'>
							Hủy
						</Button>
						<Button type='submit' color='primary'>
							Đăng ký
						</Button>
					</DialogActions>
				</form>
			</Dialog>
		</Grid>
	);
}
