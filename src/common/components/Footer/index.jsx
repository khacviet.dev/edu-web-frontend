import { Container, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import infoApi from 'api/infoApi';
import React, { useContext, useEffect, useState } from 'react';
import { ToastifyContext } from 'utils/ToastifyConfig';

const useStyles = makeStyles((theme) => ({
	title: {
		flexGrow: 1,
		cursor: 'pointer',
	},
	fontSizeTitle: {
		fontSize: '18px',
		color: '#fff',
	},

	footerInfo: {
		fontSize: '14px',
		lineHeight: '1.5rem',
		color: '#ccc !important',
	},
}));

function Footer(props) {
	const classes = useStyles();
	const [footer, setFooter] = useState();
	const notify = useContext(ToastifyContext);
	// get footer
	useEffect(() => {
		const getSliders = async () => {
			try {
				const response = await infoApi.getFooter();
				const data = response;
				setFooter(data);
			} catch (error) {
				notify('error', error.response.data.message);
			}
		};
		getSliders();
	}, []);

	return (
		<Container
			style={{ marginTop: '3rem', marginBottom: '3rem' }}
			maxWidth='lg'
			container
		>
			<Grid container item md={12} spacing={2}>
				<Grid item sm={12} md={6} className={classes.fontSizeTitle}>
					<Grid container>Giới thiệu trung tâm</Grid>
					<Grid container>
						{footer && (
							<div className={classes.footerInfo}>
								{footer.shortDes && footer.shortDes[0].description}
							</div>
						)}
					</Grid>
				</Grid>
				<Grid item sm={12} md={3} className={classes.fontSizeTitle}>
					<Grid container>Tư vấn viên</Grid>
					<Grid container>
						<div className={classes.footerInfo}>
							{footer &&
								footer.consultant &&
								footer.consultant.map((consultant, index) => (
									<Grid
										key={`consultant${index}`}
										item
										container
										md={12}
										className={classes.footerInfo}
									>
										<Grid item md={12}>
											{`${consultant.full_name} - ${consultant.phone}`}
										</Grid>
										<Grid item md={12}>
											{`${consultant.email}`}
										</Grid>
									</Grid>
								))}
						</div>
					</Grid>
				</Grid>
				<Grid item sm={12} md={3} className={classes.fontSizeTitle}>
					<Grid container>Địa chỉ</Grid>
					<Grid container>
						<div className={classes.footerInfo}>
							{footer &&
								footer.address &&
								footer.address.map((address, index) => (
									<Grid
										key={`address${index}`}
										item
										container
										md={12}
										className={classes.footerInfo}
									>
										{`${address.setting_value} : ${address.description}`}
									</Grid>
								))}
						</div>
					</Grid>
				</Grid>
			</Grid>
		</Container>
	);
}

export default Footer;
