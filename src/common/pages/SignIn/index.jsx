import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import authApi from 'api/authApi';
import si from 'assets/images/si.jpg';
import x from 'assets/logos/xoai.png';
import InputField from 'custom-fields/InputField';
import { FastField, Form, Formik } from 'formik';
import React, { useContext, useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link, useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as Yup from 'yup';

const useStyles = makeStyles((theme) => ({
	root: {
		height: '100vh',
	},
	image: {
		backgroundImage: `url(${si})`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
		backgroundPosition: 'center',
	},
	paper: {
		margin: theme.spacing(8, 4),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '80%',
		[theme.breakpoints.down('xs')]: {
			width: '100%',
		},
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

export default function SignIn() {
	const notify = useContext(ToastifyContext);
	const classes = useStyles();
	let history = useHistory();
	const [loading, setLoading] = useState(false);

	const initialValues = {
		username: '',
		password: '',
	};

	const validationSchema = Yup.object().shape({
		username: Yup.string().required('Bạn cần nhập mục này'),
		password: Yup.string().required('Required'),
	});

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={(values) => {
				const post = async () => {
					try {
						setLoading(true);
						await authApi.signin(values);
						setLoading(false);
						if (localStorage.getItem('courseLink') !== null) {
							const courselink = localStorage.getItem('courseLink');
							history.push({
								pathname: `/coursedetail`,
								state: {
									course_id: courselink,
								},
							});

							localStorage.removeItem('courseLink');
							return;
						}
						history.push('/');
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
					}
				};
				post();
			}}
		>
			{(formikProps) => {
				return (
					<Grid container component='main' className={classes.root}>
						{loading && (
							<div
								style={{
									position: 'fixed',
									top: '50%',
									left: '50%',
									width: '50px',
									transform: 'translate(-50%, -50%)',
									zIndex: '1000000000',
								}}
								className='cp-spinner cp-balls'
							></div>
						)}
						<Helmet>
							<title>Đăng nhập | Xoài Education</title>
						</Helmet>
						<Grid
							item
							xs={false}
							sm={false}
							md={7}
							lg={7}
							className={classes.image}
						/>
						<Grid
							item
							xs={12}
							sm={12}
							md={5}
							lg={5}
							component={Paper}
							elevation={6}
							square
						>
							<Button
								style={{ margin: '0.5rem 0 0 0.5rem' }}
								startIcon={<ArrowBackIcon />}
								onClick={() => {
									history.push('/');
								}}
							>
								Về trang chủ
							</Button>
							<div className={classes.paper}>
								<img
									src={x}
									style={{
										width: '100px',
										height: 'auto',
										marginBottom: '0.5rem',
									}}
									loading='lazy'
								/>
								<Typography
									component='h1'
									variant='h5'
									style={{ fontWeight: 'bold' }}
								>
									Đăng nhập
								</Typography>
								<Form className={classes.form}>
									<FastField
										name='username'
										component={InputField}
										label='Tài khoản'
										autoFocus={false}
										fullWidth={true}
										required={true}
									/>
									<FastField
										name='password'
										component={InputField}
										label='Mật khẩu'
										autoFocus={false}
										fullWidth={true}
										required={true}
										type='password'
									/>
									<Grid container style={{ marginTop: '0.5rem' }}>
										<Grid item md={6} style={{ width: '50%' }}>
											<Link
												to='/reset'
												variant='body2'
												style={{ color: '#555' }}
											>
												Quên mật khẩu ?
											</Link>
										</Grid>
										<Grid item md={6} style={{ width: '50%' }}>
											<Grid container justify='flex-end'>
												<Link
													to='/signup'
													variant='body2'
													style={{ color: '#555' }}
												>
													Chưa có tài khoản ?
												</Link>
											</Grid>
										</Grid>
									</Grid>
									<Button
										type='submit'
										fullWidth
										variant='contained'
										color='primary'
										className={classes.submit}
									>
										Đăng nhập
									</Button>
								</Form>
							</div>
						</Grid>
					</Grid>
				);
			}}
		</Formik>
	);
}
