const { makeStyles } = require('@material-ui/core');
import si from 'assets/images/si.jpg';

export const useStyles = makeStyles((theme) => ({
	root: {
		height: '100vh',
	},
	image: {
		backgroundImage: `url(${si})`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
		backgroundPosition: 'center',
	},
	paper: {
		margin: theme.spacing(8, 4),
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '70%',
		[theme.breakpoints.down('xs')]: {
			width: '85%',
		},
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
	emailTo: {
		[theme.breakpoints.down('xs')]: {
			marginTop: '1rem',
		},
		[theme.breakpoints.up('sm')]: {
			paddingLeft: '1rem',
		},
	},
	description: {
		width: '70%',
		[theme.breakpoints.down('xs')]: {
			width: '85%',
		},
	},
	panel: {
		paddingTop: '6rem',
		[theme.breakpoints.down('xs')]: {
			paddingTop: '4rem',
		},
	},
}));
