import { CircularProgress } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import authApi from 'api/authApi';
import InputField from 'custom-fields/InputField';
import { FastField, Form, Formik } from 'formik';
import React, { useContext, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory, useParams } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as Yup from 'yup';
import { useStyles } from './ChangePasswordStyle';

export default function ChangePassword() {
	const notify = useContext(ToastifyContext);
	const [loading, setLoading] = useState(false);
	const { userId } = useParams();

	const classes = useStyles();
	let history = useHistory();

	const backToSignIn = () => {
		history.push('/signin');
	};

	const initialValues = {
		newpassword: '',
	};

	const validationSchema = Yup.object().shape({
		newpassword: Yup.string()
			.required('Bạn cần nhập mục này')
			.min(6, 'Mật khẩu phải có ít nhất 6 kí tự')
			.max(100, 'Mật khẩu quá dài'),
	});

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={(values) => {
				const post = async () => {
					try {
						setLoading(true);
						const response = await authApi.changePassword(userId, values);
						setLoading(false);
						notify('success', response.message);
						history.push('/signin');
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
					}
				};
				post();
			}}
		>
			{(formikProps) => {
				return (
					<Grid container component='main' className={classes.root}>
						<Helmet>
							<title>Nhập mật khẩu mới | Xoài Education</title>
						</Helmet>
						<Grid item xs={false} sm={false} md={7} className={classes.image} />
						<Grid
							item
							xs={12}
							sm={12}
							md={5}
							component={Paper}
							elevation={6}
							square
						>
							<Grid
								container
								justify='center'
								style={{ height: '40px', marginTop: '40px' }}
							>
								{loading && <CircularProgress />}
							</Grid>
							<Grid container className={classes.panel}>
								<Grid container justify='center'>
									<Typography
										variant='h5'
										style={{
											marginBottom: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										Chọn một mật khẩu mới
									</Typography>
								</Grid>
								<Grid container justify='center'>
									<Typography variant='body1' className={classes.description}>
										Tạo mật khẩu mới dài ít nhất 6 ký tự. Mật khẩu mạnh có sự
										kết hợp của các chữ cái, chữ số và dấu chấm câu.
									</Typography>
								</Grid>
								<Grid container justify='center'>
									<Form className={classes.form}>
										<FastField
											name='newpassword'
											component={InputField}
											label='Mật khẩu mới'
											fullWidth={true}
											required={true}
											type='password'
										/>

										<Grid
											container
											className={classes.submit}
											justify='flex-end'
										>
											<Button
												onClick={backToSignIn}
												variant='contained'
												color='default'
												style={{
													marginRight: '1rem',
													textTransform: 'none',
												}}
											>
												Bỏ qua
											</Button>
											<Button
												type='submit'
												variant='contained'
												color='primary'
												style={{
													textTransform: 'none',
												}}
											>
												Tiếp tục
											</Button>
										</Grid>
									</Form>
								</Grid>
							</Grid>
						</Grid>
					</Grid>
				);
			}}
		</Formik>
	);
}
