const { makeStyles } = require('@material-ui/core');
import si from 'assets/images/si.jpg';

export const useStyles = makeStyles((theme) => ({
	root: {
		height: '100vh',
	},
	image: {
		backgroundImage: `url(${si})`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
		backgroundPosition: 'center',
	},
	paper: {
		margin: theme.spacing(8, 4),
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '80%',
		[theme.breakpoints.down('xs')]: {
			width: '85%',
		},
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
		[theme.breakpoints.up('sm')]: {
			paddingRight: '1rem',
		},
	},
	emailTo: {
		[theme.breakpoints.down('xs')]: {
			marginTop: '1rem',
		},
		[theme.breakpoints.up('sm')]: {
			paddingLeft: '2rem',
		},
	},
	description: {
		width: '80%',
		[theme.breakpoints.down('xs')]: {
			width: '85%',
		},
	},
	panel: {
		paddingTop: '8rem',
		[theme.breakpoints.down('xs')]: {
			paddingTop: '6rem',
		},
	},
}));
