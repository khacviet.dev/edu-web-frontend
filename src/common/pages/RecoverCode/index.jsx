import { CircularProgress } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import authApi from 'api/authApi';
import InputField from 'custom-fields/InputField';
import { FastField, Form, Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory, useParams } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as Yup from 'yup';
import { useStyles } from './RecoverCode';

export default function RecoverCode(props) {
	const notify = useContext(ToastifyContext);
	const { username } = props.location.state;
	const [loading, setLoading] = useState(false);
	const [isRenew, setIsRenew] = useState(false);

	const [count, setCount] = useState(180);

	const { userId } = useParams();

	const classes = useStyles();
	let history = useHistory();

	const backToSignIn = () => {
		history.push('/signin');
	};

	const initialValues = {
		code: '',
	};

	const validationSchema = Yup.object().shape({
		code: Yup.string()
			.required('Bạn cần nhập mục này')
			.matches(/^[0-9]+$/, 'Mã chỉ bao gồm chữ số'),
	});

	const renewCode = () => {
		const post = async () => {
			try {
				setLoading(true);
				const data = {
					username,
				};
				const response = await authApi.resetPassword(data);
				setLoading(false);
				setCount(180);
				setIsRenew(!isRenew);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};
		post();
	};

	useEffect(() => {
		const myInteval = setInterval(() => {
			setCount((prevState) => {
				if (prevState > 1) {
					return prevState - 1;
				}
				return 'Mã đã hết hạn';
			});
		}, 1000);

		return () => clearInterval(myInteval);
	}, [isRenew]);

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={(values) => {
				const post = async () => {
					try {
						setLoading(true);
						const response = await authApi.checkRecoverCode(userId, values);
						setLoading(false);
						history.push(`/reset/newpassword/${userId}`);
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
					}
				};
				post();
			}}
		>
			{(formikProps) => {
				return (
					<Grid container component='main' className={classes.root}>
						<Helmet>
							<title>Mã khôi phục | Xoài Education</title>
						</Helmet>
						<Grid item xs={false} sm={false} md={7} className={classes.image} />
						<Grid
							item
							xs={12}
							sm={12}
							md={5}
							component={Paper}
							elevation={6}
							square
						>
							<Grid
								container
								justify='center'
								style={{ height: '40px', marginTop: '40px' }}
							>
								{loading && <CircularProgress />}
							</Grid>
							<Grid container className={classes.panel}>
								<Grid container justify='center'>
									<Typography
										variant='h5'
										style={{
											marginBottom: '1.5rem',
											fontWeight: 'bold',
										}}
									>
										{`Nhập mã bảo mật (${count})`}
									</Typography>
								</Grid>
								<Grid container justify='center'>
									<Typography variant='body1' className={classes.description}>
										Vui lòng kiểm tra email của bạn để tìm tin nhắn có mã của
										bạn. Mã của bạn dài 6 số.
									</Typography>
								</Grid>
								<Grid container justify='center'>
									<Form className={classes.form}>
										<Grid container>
											<Grid item xs={12} sm={6} md={6} lg={6}>
												<FastField
													name='code'
													component={InputField}
													label='Nhập mã'
													fullWidth={true}
													required={true}
												/>
											</Grid>
											<Grid item xs={12} sm={6} md={6} lg={6}>
												<Grid
													container
													style={{ height: '100%' }}
													alignItems='center'
													className={classes.emailTo}
												>
													<Typography
														variant='body1'
														component='div'
														style={{ fontSize: '0.9rem' }}
													>
														<b>Chúng tôi đã gửi mã của bạn đến:</b>
														<br />
														<div
															style={{
																fontSize: '0.8rem',
																marginTop: '0.25rem',
															}}
														>
															{username}
														</div>
													</Typography>
												</Grid>
											</Grid>
										</Grid>

										<Grid
											container
											className={classes.submit}
											justify='flex-end'
										>
											<Button
												startIcon={<AutorenewIcon />}
												onClick={renewCode}
												variant='contained'
												color='default'
												style={{
													marginRight: '1rem',
													textTransform: 'none',
												}}
											>
												Gửi lại mã
											</Button>
											<Button
												onClick={backToSignIn}
												variant='contained'
												color='secondary'
												style={{
													marginRight: '1rem',
													textTransform: 'none',
												}}
											>
												Hủy bỏ
											</Button>
											<Button
												type='submit'
												variant='contained'
												color='primary'
												style={{
													textTransform: 'none',
												}}
											>
												Tiếp tục
											</Button>
										</Grid>
									</Form>
								</Grid>
							</Grid>
						</Grid>
					</Grid>
				);
			}}
		</Formik>
	);
}
