import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import authApi from 'api/authApi';
import si from 'assets/images/si.jpg';
import x from 'assets/logos/xoai.png';
import InputField from 'custom-fields/InputField';
import { FastField, Form, Formik } from 'formik';
import React, { useContext, useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link, useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as Yup from 'yup';

const useStyles = makeStyles((theme) => ({
	root: {
		height: '100vh',
	},
	image: {
		backgroundImage: `url(${si})`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
		backgroundPosition: 'center',
	},
	paper: {
		margin: theme.spacing(2, 4),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '80%',
		[theme.breakpoints.down('xs')]: {
			width: '100%',
		},
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

export default function SignUp() {
	const notify = useContext(ToastifyContext);
	const classes = useStyles();
	let history = useHistory();
	const [loading, setLoading] = useState(false);
	const [isSignup, setisSignup] = useState(true);

	const initialValues = {
		username: '',
		password: '',
		email: '',
		phone: '',
		fullname: '',
	};

	const validationSchema = Yup.object().shape({
		fullname: Yup.string().required('Bạn cần nhập mục này'),
		username: Yup.string().required('Bạn cần nhập mục này'),
		email: Yup.string()
			.email('Email không hợp lệ')
			.required('Bạn cần nhập mục này'),
		phone: Yup.string()
			.matches(/^[0-9]+$/, 'Số điện thoại chỉ bao gồm chữ số')
			.min(10, 'Số điện thoại gồm ít nhất 10 số')
			.max(11, 'Số điện thoại gồm nhiều nhất 11 số')
			.required('Bạn cần nhập mục này'),

		password: Yup.string().required('Bạn cần nhập mục này'),
	});

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={(values) => {
				const post = async () => {
					try {
						setLoading(true);
						const response = await authApi.signup(values);
						setLoading(false);
						setisSignup(false);
						notify('success', response.message);
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
					}
				};
				post();
			}}
		>
			{(formikProps) => {
				return (
					<Grid container component='main' className={classes.root}>
						{loading && (
							<div
								style={{
									position: 'fixed',
									top: '50%',
									left: '50%',
									width: '50px',
									transform: 'translate(-50%, -50%)',
									zIndex: '1000000000',
								}}
								className='cp-spinner cp-balls'
							></div>
						)}
						<Helmet>
							<title>Đăng ký | Xoài CMS</title>
						</Helmet>
						<Grid
							item
							xs={false}
							sm={false}
							md={7}
							lg={7}
							className={classes.image}
						/>
						<Grid
							item
							xs={12}
							sm={12}
							md={5}
							lg={5}
							component={Paper}
							elevation={6}
							square
						>
							<Button
								style={{ margin: '0.5rem 0 0 0.5rem' }}
								startIcon={<ArrowBackIcon />}
								onClick={() => {
									history.push('/');
								}}
							>
								Về trang chủ
							</Button>
							<div className={classes.paper}>
								<img
									src={x}
									style={{
										width: '100px',
										height: 'auto',
										marginBottom: '0.5rem',
									}}
									loading='lazy'
								/>
								<Typography
									component='h1'
									variant='h5'
									style={{ fontWeight: 'bold' }}
								>
									Đăng ký
								</Typography>
								{isSignup ? (
									<Form className={classes.form}>
										<FastField
											name='fullname'
											component={InputField}
											label='Họ và tên'
											autoFocus={false}
											fullWidth={true}
											required={true}
											size='small'
										/>
										<FastField
											name='username'
											component={InputField}
											label='Tài khoản'
											autoFocus={false}
											fullWidth={true}
											required={true}
											size='small'
										/>
										<FastField
											name='password'
											component={InputField}
											label='Mật khẩu'
											autoFocus={false}
											fullWidth={true}
											required={true}
											type='password'
											size='small'
										/>
										<FastField
											name='email'
											component={InputField}
											label='Email'
											autoFocus={false}
											fullWidth={true}
											required={true}
											size='small'
										/>
										<FastField
											name='phone'
											component={InputField}
											label='Số điện thoại'
											autoFocus={false}
											fullWidth={true}
											required={true}
											size='small'
										/>

										<Grid container style={{ marginTop: '0.5rem' }}>
											<Link
												to='/signin'
												variant='body2'
												style={{ color: '#555' }}
											>
												Đã có tài khoản ?
											</Link>
										</Grid>
										<Button
											type='submit'
											fullWidth
											variant='contained'
											color='primary'
											className={classes.submit}
										>
											Đăng ký
										</Button>
									</Form>
								) : (
									<Grid container style={{ marginTop: '2rem' }}>
										<Grid container justify='center'>
											<Typography
												variant='h6'
												style={{
													marginBottom: '1.5rem',
													fontWeight: 'bold',
												}}
											>
												Một email xác nhận vừa được gửi đến địa chỉ email của
												bạn
											</Typography>
										</Grid>
										<Grid container justify='center'>
											<Typography variant='body1'>
												Vui lòng xác nhận để bắt đầu trải nghiệm các dịch vụ của
												Xoài Education
											</Typography>
										</Grid>
									</Grid>
								)}
							</div>
						</Grid>
					</Grid>
				);
			}}
		</Formik>
	);
}
