import { Breadcrumbs, Container, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Footer from 'common/components/Footer';
import MyAppBar from 'common/components/MyNavBar';
import 'csspin/csspin.css';
import React from 'react';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		position: 'relative',
		minHeight: '100vh',
	},
	refer: {
		fontSize: '1.1rem',
		color: 'inherit',
		textDecoration: 'none',
		cursor: 'pointer',
	},
	footer: {
		backgroundColor: '#007CBE',
		position: 'absolute',
		bottom: '0',
		width: '100%',
		height: 'fit-content',
	},
}));

export default function MasterLayout(props) {
	const classes = useStyles();
	const { referer, header, breadcrumbs } = props;
	return (
		<div className={classes.root}>
			<MyAppBar />
			<Container
				style={{
					marginTop: '3rem',
					minHeight: '500px',
					paddingBottom: '20rem',
				}}
				maxWidth='lg'
			>
				{breadcrumbs == true && (
					<Grid container='true' style={{ marginBottom: '1rem' }}>
						<Breadcrumbs>
							<Link className={classes.refer} to='/'>
								Trang chủ
							</Link>
							{referer &&
								referer.map((r, index) => (
									<Link
										key={index}
										className={classes.refer}
										to={{
											pathname: `/${r.value}`,
											state: {
												title: `${r.state}`,
												course_id: `${r.course_id}`,
											},
										}}
									>
										{r.display}
									</Link>
								))}

							{header && (
								<Typography style={{ fontSize: '1.1rem' }} color='textPrimary'>
									{header}
								</Typography>
							)}
						</Breadcrumbs>
					</Grid>
				)}
				{props.children}
			</Container>
			<Grid container='true' className={classes.footer}>
				<Footer />
			</Grid>
		</div>
	);
}
