import { CircularProgress } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import authApi from 'api/authApi';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import { useStyles } from './ConfirmMail';
const queryString = require('query-string');

export default function ConfirmMail(props) {
	const notify = useContext(ToastifyContext);
	const classes = useStyles();
	let history = useHistory();
	const [isConfirm, setIsConfirm] = useState(false);

	const { code, email } = queryString.parse(props.location.search);

	const [loading, setLoading] = useState(false);

	useEffect(() => {
		const post = async () => {
			try {
				setLoading(true);
				const data = { code, email };
				const response = await authApi.confirmEmail(data);
				setIsConfirm(true);
				const myTimeOut = setTimeout(() => {
					setLoading(false);
					history.push('/signin');
					clearTimeout(myTimeOut);
				}, 2000);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};

		post();
	}, []);

	return (
		<Grid container component='main' className={classes.root}>
			<Helmet>
				<title>Xác nhận Email | Xoài Education</title>
			</Helmet>
			<Grid
				item
				xs={false}
				sm={false}
				md={7}
				lg={7}
				className={classes.image}
			/>
			<Grid
				item
				xs={12}
				sm={12}
				md={5}
				lg={5}
				component={Paper}
				elevation={6}
				square
			>
				<Grid
					container
					justify='center'
					style={{ height: '40px', marginTop: '40px' }}
				>
					{loading && <CircularProgress />}
				</Grid>
				{!isConfirm ? (
					<Grid container className={classes.panel}>
						<Grid container justify='center'>
							<Typography
								variant='h6'
								style={{
									marginBottom: '1.5rem',
									fontWeight: 'bold',
								}}
							>
								Một email xác nhận vừa được gửi đến địa chỉ email của bạn
							</Typography>
						</Grid>
						<Grid container justify='center'>
							<Typography variant='body1' className={classes.description}>
								Vui lòng xác nhận để bắt đầu trải nghiệm các dịch vụ của Xoài
								Education
							</Typography>
						</Grid>
					</Grid>
				) : (
					<Grid container className={classes.panel}>
						<Grid container justify='center'>
							<Typography
								variant='h6'
								style={{
									marginBottom: '1.5rem',
									fontWeight: 'bold',
								}}
							>
								Xác nhận email thành công
							</Typography>
						</Grid>
					</Grid>
				)}
			</Grid>
		</Grid>
	);
}
