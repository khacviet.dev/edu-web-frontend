const { makeStyles } = require('@material-ui/core');
import si from 'assets/images/si.jpg';

export const useStyles = makeStyles((theme) => ({
	root: {
		height: '100vh',
	},
	image: {
		backgroundImage: `url(${si})`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
		backgroundPosition: 'center',
	},
	paper: {
		margin: theme.spacing(8, 4),
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '70%',
		[theme.breakpoints.down('xs')]: {
			width: '90%',
		},
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
	description: {
		[theme.breakpoints.down('xs')]: {
			padding: '0 1.5rem',
		},
	},
	panel: {
		marginTop: '7rem',
		[theme.breakpoints.down('xs')]: {
			marginTop: '3rem',
		},
	},
}));
