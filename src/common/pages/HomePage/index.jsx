import { Grid, Paper, Typography } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import VisibilityIcon from '@material-ui/icons/Visibility';
import courseApi from 'api/courseApi';
import infoApi from 'api/infoApi';
import postApi from 'api/postApi';
import MySlide from 'common/components/MySlide';
import CourseCard from 'features/Course/components/CourseCard';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { ToastifyContext } from 'utils/ToastifyConfig';
import MasterLayout from '../MasterLayout';

function HomePage() {
	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.down('sm'));
	let history = useHistory();
	const notify = useContext(ToastifyContext);
	const [isLoading, setIsLoading] = useState(false);
	const [sliders, setSliders] = useState([]);
	const [posts, setPost] = useState([]);
	const [courses, setCourse] = useState([]);
	const [mostBuy, setMostBuy] = useState([]);

	// get sliders
	useEffect(() => {
		const getSliders = async () => {
			try {
				setIsLoading(true);
				const response = await infoApi.getSliders();
				const data = response.data;
				setSliders(data);
				setIsLoading(false);
			} catch (error) {
				setIsLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getSliders();
	}, []);

	// get 4 new post
	useEffect(() => {
		const getFourNewPost = async () => {
			try {
				setIsLoading(true);
				const response = await postApi.getFourNewPost();
				const data = response.data;
				setPost(data);
				setIsLoading(false);
			} catch (error) {
				setIsLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getFourNewPost();
	}, []);

	// get 4 new course featture
	useEffect(() => {
		const getFeatureCourse = async () => {
			try {
				setIsLoading(true);
				const response = await courseApi.getFeatureCourse();
				const data = response.data;
				setCourse(data);
				setIsLoading(false);
			} catch (error) {
				setIsLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getFeatureCourse();
	}, []);

	// get 4 most buy
	useEffect(() => {
		const getMostBuyCourse = async () => {
			try {
				setIsLoading(true);
				const response = await courseApi.getMostBuyCourse();
				const data = response.data;
				setMostBuy(data);
				setIsLoading(false);
			} catch (error) {
				setIsLoading(false);
				notify('error', error.response.data.message);
			}
		};
		getMostBuyCourse();
	}, []);

	const handleFourPost = (post_id) => {
		history.push({
			pathname: `/postdetail/${post_id}`,
		});
	};

	return (
		<MasterLayout>
			{isLoading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						width: '50px',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-balls'
				></div>
			)}
			<Helmet>
				<title>Xoài Education</title>
			</Helmet>
			<Grid container spacing={matches ? 0 : 4}>
				<Grid item sm={12} md={8} lg={8}>
					{sliders.length > 0 && (
						<Grid container justify='center'>
							<MySlide data={sliders}></MySlide>
						</Grid>
					)}
				</Grid>
				<Grid item sm={12} md={4} lg={4}>
					<Grid container style={{ marginTop: matches ? '1rem' : '0' }}>
						{posts.length > 0 &&
							posts.map((post, index) => (
								<Paper
									key={`postHome${index}`}
									style={{
										cursor: 'pointer',
										width: '100%',
										height: '105px',
										marginBottom: '0.75rem',
									}}
									onClick={() => handleFourPost(post.post_id)}
								>
									<Grid container>
										<Grid item sm={3} md={3} lg={3} style={{ width: '33.33%' }}>
											<img
												style={{
													width: '100%',
													height: '105px',
													borderTopLeftRadius: '5px',
													borderBottomLeftRadius: '5px',
												}}
												src={post.thumbnail}
												alt='b'
												loading='lazy'
											/>
										</Grid>
										<Grid
											item
											sm={9}
											md={9}
											lg={9}
											style={{ padding: '0.5rem', width: '66.66%' }}
										>
											<Grid
												container='true'
												direction='column'
												justify='space-between'
												style={{ height: '100%' }}
											>
												<Typography variant='body1'>
													{post.title.length > 72
														? post.title.substring(0, 72) + '...'
														: post.title}
												</Typography>
												<Typography variant='body2' style={{ color: '#555' }}>
													{post.full_name}
												</Typography>
											</Grid>
										</Grid>
									</Grid>
								</Paper>
							))}
					</Grid>
				</Grid>
			</Grid>

			<Grid container='true' style={{ marginTop: '6rem' }}>
				<Grid
					container='true'
					alignItems='center'
					style={{ marginBottom: '1rem' }}
				>
					<TrendingUpIcon />
					<Typography variant='h5' style={{ fontWeight: 'bold' }}>
						&nbsp;&nbsp;&nbsp;Các khóa học nổi bật
					</Typography>
				</Grid>
				<Grid container='true' spacing={3}>
					{courses.length > 0 &&
						courses.map((data, index) => (
							<Grid key={`featureCourse${index}`} item md={3}>
								<CourseCard data={data}></CourseCard>
							</Grid>
						))}
				</Grid>
			</Grid>
			<Grid container='true' style={{ marginTop: '6rem' }}>
				<Grid
					container='true'
					alignItems='center'
					style={{ marginBottom: '1.5rem' }}
				>
					<VisibilityIcon />
					<Typography variant='h5' style={{ fontWeight: 'bold' }}>
						&nbsp;&nbsp;&nbsp;Các khóa học mua nhiều nhất
					</Typography>
				</Grid>
				<Grid container='true' spacing={3}>
					{mostBuy.length > 0 &&
						mostBuy.map((data, index) => (
							<Grid key={`featureCourseMostBuy${index}`} item md={3}>
								<CourseCard data={data}></CourseCard>
							</Grid>
						))}
				</Grid>
			</Grid>
		</MasterLayout>
	);
}

export default HomePage;
