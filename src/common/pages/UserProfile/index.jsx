import {
	Avatar,
	Badge,
	Button,
	FormControl,
	Grid,
	Hidden,
	MenuItem,
	Paper,
	Select,
	TextField,
	Typography,
	withStyles,
} from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ArtTrackIcon from '@material-ui/icons/ArtTrack';
import FaceIcon from '@material-ui/icons/Face';
import HomeIcon from '@material-ui/icons/Home';
import MailIcon from '@material-ui/icons/Mail';
import PhoneIcon from '@material-ui/icons/Phone';
import userApi from 'api/userApi';
import ChangeAvatarModal from 'common/components/ChangeAvatarModal';
import InputField from 'custom-fields/InputField';
import { getAvatar, updateAvatar } from 'features/User/avatarSlice';
import { FastField, Form, Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as yup from 'yup';
import MasterLayout from '../MasterLayout';
const { makeStyles, useTheme } = require('@material-ui/core');
export const useStyles = makeStyles((theme) => ({
	paper: {
		padding: theme.spacing(3, 2, 3, 2),
		height: 'fit-content',
	},
}));

const SmallAvatar = withStyles((theme) => ({
	root: {
		width: 22,
		height: 22,
		padding: '0.9rem',
	},
}))(Avatar);

const parseRole = (role) => {
	switch (role) {
		case '1':
			return 'ADMINISTRATOR';
		case '2':
			return 'QUẢN LÝ';
		case '3':
			return 'GIÁO VIÊN';
		case '4':
			return 'TRỢ GIẢNG';
		case '5':
			return 'HỌC SINH';
		case '6':
			return 'MAKETING';
	}
};

function UserProfile(props) {
	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.down('sm'));
	const notify = useContext(ToastifyContext);

	const classes = useStyles();

	const [profile, setProfile] = useState({});
	const avatars = useSelector((state) => state.avatars);
	const dispatch = useDispatch();

	const handleProfileChange = (e) => {
		setProfile({
			...profile,
			[e.target.name]: e.target.value,
		});
	};
	const [loading, setLoading] = useState(false);

	const handleChangeAvatar = (data) => {
		dispatch(updateAvatar(data));
	};

	useEffect(() => {
		const getProfile = async () => {
			try {
				setLoading(true);
				const response = await userApi.getUserById();
				setLoading(false);
				setProfile(response);
			} catch (error) {
				setLoading(false);
				notify('error', error.response.data.message);
			}
		};

		getProfile();
	}, []);

	useEffect(() => {
		dispatch(getAvatar());
	}, []);

	const validationSchema = yup.object().shape({
		full_name: yup.string().required('Trường không được để trống'),
		display_name: yup.string().required('Trường không được để trống'),
		email: yup.string().email('Email không hợp lệ'),
		phone: yup
			.string()
			.matches(/^[0-9]+$/, 'Số điện thoại chỉ bao gồm chữ số')
			.min(10, 'Số điện thoại gồm ít nhất 10 số')
			.max(12, 'Số điện thoại gồm nhiều nhất 11 số'),
		address: yup.string(),
	});

	return (
		<Formik
			initialValues={profile}
			enableReinitialize
			validationSchema={validationSchema}
			onSubmit={(values) => {
				const post = async () => {
					try {
						setLoading(true);
						const response = await userApi.updateProfile(values);
						setLoading(false);
						notify('success', response.message);
					} catch (error) {
						setLoading(false);
						notify('error', error.response.data.message);
					}
				};
				post();
			}}
		>
			{(formikProps) => {
				return (
					<MasterLayout>
						<Grid
							container
							spacing={4}
							direction={matches ? 'column-reverse' : 'row'}
						>
							{(avatars.loading || loading) && (
								<div
									style={{
										position: 'fixed',
										top: '50%',
										left: '50%',
										width: '50px',
										transform: 'translate(-50%, -50%)',
										zIndex: '1000000000',
									}}
									className='cp-spinner cp-balls'
								></div>
							)}
							<Helmet>
								<title>Trang cá nhân | Xoài CMS</title>
							</Helmet>
							<Grid item sm={12} md={8} lg={8}>
								<Form>
									<Paper className={classes.paper}>
										<Grid container alignItems='center'>
											<Hidden smDown>
												<Grid item md={6} lg={6}>
													<Grid container alignItems='center'>
														<AccountCircleIcon
															style={{ marginRight: '1rem' }}
														/>
														Tài khoản
													</Grid>
												</Grid>
											</Hidden>
											<Grid item sm={12} md={6} lg={6} container>
												<TextField
													fullWidth
													value={profile.username}
													variant='outlined'
													size='small'
													margin='normal'
													disabled
												/>
											</Grid>
										</Grid>
										<Grid container alignItems='center'>
											<Hidden smDown>
												<Grid item md={6} lg={6}>
													<Grid container alignItems='center'>
														<FaceIcon style={{ marginRight: '1rem' }} />
														Họ và tên
													</Grid>
												</Grid>
											</Hidden>
											<Grid item sm={12} md={6} lg={6} container>
												<FastField
													name='full_name'
													component={InputField}
													fullWidth
													size='small'
													required
												/>
											</Grid>
										</Grid>

										<Grid container alignItems='center'>
											<Hidden smDown>
												<Grid item md={6} lg={6}>
													<Grid container alignItems='center'>
														<FaceIcon style={{ marginRight: '1rem' }} />
														Giới tính
													</Grid>
												</Grid>
											</Hidden>

											<Grid item sm={12} md={6} lg={6} container>
												{profile.gender !== undefined && (
													<FormControl
														margin='normal'
														style={{
															minWidth: '120px',
														}}
														size='small'
													>
														<Select
															name='gender'
															variant='outlined'
															value={profile.gender}
															onChange={handleProfileChange}
														>
															<MenuItem value={''}>None</MenuItem>
															<MenuItem value={'male'}>Nam</MenuItem>
															<MenuItem value={'female'}>Nữ</MenuItem>
														</Select>
													</FormControl>
												)}
											</Grid>
										</Grid>
										<Grid container alignItems='center'>
											<Hidden smDown>
												<Grid item md={6} lg={6}>
													<Grid container alignItems='center'>
														<ArtTrackIcon style={{ marginRight: '1rem' }} />
														Tên hiển thị
													</Grid>
												</Grid>
											</Hidden>
											<Grid item sm={12} md={6} lg={6} container>
												<FastField
													name='display_name'
													component={InputField}
													fullWidth
													size='small'
													required
												/>
											</Grid>
										</Grid>
										<Grid container alignItems='center'>
											<Hidden smDown>
												<Grid item md={6} lg={6}>
													<Grid container alignItems='center'>
														<MailIcon style={{ marginRight: '1rem' }} />
														Email
													</Grid>
												</Grid>
											</Hidden>
											<Grid sm={12} item md={6} lg={6} container>
												<FastField
													name='email'
													component={InputField}
													fullWidth
													size='small'
													required
												/>
											</Grid>
										</Grid>
										<Grid container alignItems='center'>
											<Hidden smDown>
												<Grid item md={6} lg={6}>
													<Grid container alignItems='center'>
														<PhoneIcon style={{ marginRight: '1rem' }} />
														Số điện thoại
													</Grid>
												</Grid>
											</Hidden>
											<Grid item sm={12} md={6} lg={6} container>
												<FastField
													name='phone'
													component={InputField}
													fullWidth
													size='small'
												/>
											</Grid>
										</Grid>
										<Grid container alignItems='center'>
											<Hidden smDown>
												<Grid item md={6} lg={6}>
													<Grid container alignItems='center'>
														<HomeIcon style={{ marginRight: '1rem' }} />
														Địa chỉ
													</Grid>
												</Grid>
											</Hidden>
											<Grid item sm={12} md={6} lg={6} container>
												<FastField
													name='address'
													component={InputField}
													fullWidth
													size='small'
												/>
											</Grid>
										</Grid>
										<Grid
											container
											justify='center'
											style={{ marginTop: '2rem' }}
										>
											<Button type='submit' variant='contained' color='primary'>
												Lưu thay đổi
											</Button>
										</Grid>
									</Paper>
								</Form>
							</Grid>
							<Grid item sm={12} md={4} lg={4}>
								<Paper className={classes.paper}>
									<Grid container justify='center'>
										<Badge
											overlap='circle'
											anchorOrigin={{
												vertical: 'bottom',
												horizontal: 'right',
											}}
											badgeContent={
												<SmallAvatar>
													<ChangeAvatarModal
														handleChangeAvatar={handleChangeAvatar}
													/>
												</SmallAvatar>
											}
										>
											<Avatar
												style={{
													width: '8rem',
													height: '8rem',
													boxShadow:
														'0 16px 38px -12px rgb(0 0 0 / 16%), 0 4px 25px 0 rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%)',
												}}
												src={avatars.avtLink}
											/>
										</Badge>
									</Grid>
									<Grid
										container
										justify='center'
										style={{ marginTop: '0.75rem', color: '#999' }}
									>
										<Typography style={{ fontSize: '14px' }}>
											{parseRole(profile.role_id)}
										</Typography>
									</Grid>
									<Grid
										container
										justify='center'
										style={{ marginTop: '0.75rem' }}
									>
										<Typography variant='body2' style={{ fontSize: '1.25rem' }}>
											{formikProps.values.full_name}
										</Typography>
									</Grid>
								</Paper>
							</Grid>
						</Grid>
					</MasterLayout>
				);
			}}
		</Formik>
	);
}

export default UserProfile;
