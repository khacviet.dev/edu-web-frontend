import { Button, FormControl, Grid, TextField } from '@material-ui/core';
import userApi from 'api/userApi';
import React, { useContext, useState } from 'react';
import { Helmet } from 'react-helmet';
import { ToastifyContext } from 'utils/ToastifyConfig';
import MasterLayout from '../MasterLayout';

function ChangeNewPassword() {
	const notify = useContext(ToastifyContext);

	const [loading, setLoading] = useState(false);

	const [dataInput, setDataInput] = useState({
		oldPassword: '',
		newPassword: '',
		reNewPassword: '',
	});

	const [errorInput, setErrorInput] = useState({
		oldPassword: '',
		newPassword: '',
		reNewPassword: '',
		number: 0,
	});

	const handleInputChange = (e) => {
		setDataInput({
			...dataInput,
			[e.target.name]: e.target.value,
		});
	};

	const handleNewPasswordBlur = (e) => {
		if (e.target.value === '') {
			setErrorInput({
				...errorInput,
				newPassword: 'Trường không được để trống',
				number: 1,
			});

			return;
		}

		if (e.target.value.toString().length < 6) {
			setErrorInput({
				...errorInput,
				newPassword: 'Mật khẩu phải có ít nhất 6 kí tự',
				number: 1,
			});

			return;
		}

		if (e.target.value.toString().length > 29) {
			setErrorInput({
				...errorInput,
				newPassword: 'Mật khẩu phải nhỏ hơn 30 kí tự',
				number: 1,
			});

			return;
		}

		setErrorInput({
			...errorInput,
			newPassword: '',
			number: 0,
		});
	};

	const handleReNewPasswordBlur = (e) => {
		if (e.target.value !== dataInput.newPassword) {
			setErrorInput({
				...errorInput,
				reNewPassword: 'Mật khẩu không trùng khớp',
				number: 1,
			});

			return;
		}

		setErrorInput({
			...errorInput,
			reNewPassword: '',
			number: 0,
		});
	};

	const handleChangePasswordSubmit = (e) => {
		e.preventDefault();
		if (errorInput.number === 0) {
			const post = async () => {
				try {
					setLoading(true);
					const data = {
						oldPassword: dataInput.oldPassword,
						newPassword: dataInput.newPassword,
					};

					const response = await userApi.changePassword(data);
					setLoading(false);
					notify('success', response.message);
					setDataInput({
						...dataInput,
						oldPassword: '',
						newPassword: '',
						reNewPassword: '',
					});
				} catch (error) {
					setLoading(false);
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/');
					}
				}
			};
			post();
		}
	};

	return (
		<MasterLayout header='Đổi mật khẩu'>
			<Helmet>
				<title>Đổi mật khẩu | Xoài Education</title>
			</Helmet>
			<form onSubmit={handleChangePasswordSubmit}>
				{loading && (
					<div
						style={{
							position: 'fixed',
							top: '50%',
							left: '50%',
							width: '50px',
							transform: 'translate(-50%, -50%)',
							zIndex: '1000000000',
						}}
						className='cp-spinner cp-balls'
					></div>
				)}

				<Grid container>
					<Grid container>
						<Grid item sm={false} md={4}></Grid>
						<Grid item sm={12} md={4} container>
							<FormControl fullWidth error={errorInput.oldPassword}>
								<TextField
									required
									name='oldPassword'
									value={dataInput.oldPassword}
									variant='outlined'
									margin='normal'
									error={errorInput.oldPassword}
									size='small'
									label='Mật khẩu cũ'
									type='password'
									onChange={handleInputChange}
								/>
								{errorInput.oldPassword && (
									<small style={{ color: 'red' }}>
										{errorInput.oldPassword}
									</small>
								)}
							</FormControl>
						</Grid>
						<Grid item sm={false} md={4}></Grid>
					</Grid>
					<Grid container>
						<Grid item sm={false} md={4}></Grid>
						<Grid item md={4} container>
							<FormControl fullWidth error={errorInput.newPassword}>
								<TextField
									required
									name='newPassword'
									value={dataInput.newPassword}
									variant='outlined'
									margin='normal'
									error={errorInput.newPassword}
									size='small'
									label='Mật khẩu mới'
									type='password'
									onChange={handleInputChange}
									onBlur={handleNewPasswordBlur}
								/>
								{errorInput.newPassword && (
									<small style={{ color: 'red' }}>
										{errorInput.newPassword}
									</small>
								)}
							</FormControl>
						</Grid>
						<Grid item sm={false} md={4}></Grid>
					</Grid>
					<Grid container>
						<Grid item sm={false} md={4}></Grid>
						<Grid item md={4} container>
							<FormControl fullWidth error={errorInput.reNewPassword}>
								<TextField
									required
									name='reNewPassword'
									value={dataInput.reNewPassword}
									variant='outlined'
									margin='normal'
									error={errorInput.reNewPassword}
									size='small'
									label='Nhập lại mật khẩu mới'
									type='password'
									onChange={handleInputChange}
									onBlur={handleReNewPasswordBlur}
								/>
								{errorInput.reNewPassword && (
									<small style={{ color: 'red' }}>
										{errorInput.reNewPassword}
									</small>
								)}
							</FormControl>
						</Grid>
						<Grid item sm={false} md={4}></Grid>
					</Grid>
					<Grid container style={{ marginTop: '1rem' }}>
						<Grid item sm={false} md={4}></Grid>
						<Grid item md={4} container>
							<Button
								variant='contained'
								color='primary'
								fullWidth
								type='submit'
							>
								Thay đổi
							</Button>
						</Grid>
						<Grid item sm={false} md={4}></Grid>
					</Grid>
				</Grid>
			</form>
		</MasterLayout>
	);
}

export default ChangeNewPassword;
