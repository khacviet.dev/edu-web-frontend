import React from 'react';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import './NotFound.css';

export default function NotFound() {
	return (
		<div className='panel'>
			<Helmet>
				<title>404 Not found</title>
			</Helmet>
			<div className='title'>404!</div>
			<p className='content'>The Page You're Looking For Was Not Found</p>
			<Link to='/' style={{ cursor: 'pointer' }}>
				<button style={{ cursor: 'pointer' }} className='button'>
					Go back
				</button>
			</Link>
		</div>
	);
}
