import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#007CBE',
		},
		type: 'light',
	},
	overrides: {
		MuiPaper: {
			root: {
				boxShadow: '0 1px 4px 0 rgb(0 0 0 / 14%) !important',
			},
		},
	},
});
