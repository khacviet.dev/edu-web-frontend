import axiosClient from './axiosClient';

const infoApi = {
	getSliders: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/info/getSliders`,
		});
	},
	getFooter: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/info/getFooter`,
		});
	},
};

export default infoApi;
