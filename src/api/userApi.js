import axiosClient from './axiosClient';

const userApi = {
	getUserById: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/user',
		});
	},

	getAvatar: () => {
		return axiosClient({
			method: 'GET',
			url: '/api/v1/user/avatar',
		});
	},

	changeAvatar: (params) => {
		return axiosClient({
			method: 'PATCH',
			url: '/api/v1/user/avatar',
			data: params,
		});
	},

	updateProfile: (params) => {
		return axiosClient({
			method: 'PATCH',
			url: '/api/v1/user',
			data: params,
		});
	},

	changePassword: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/user/changepassword',
			data: params,
		});
	},

	getTeachers: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/user/getTeachers`,
		});
	},

	getTeacherById: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/user/getTeacherById/${params}`,
		});
	},
	getUserInfo: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/user/getUserInfo/${params}`,
		});
	},
};

export default userApi;
