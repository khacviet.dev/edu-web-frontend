import axiosClient from './axiosClient';

const postApi = {
	getFourNewPost: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/post/getFourNewPost`,
		});
	},
	getPostCategory: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/post/getPostCategory`,
		});
	},
	getPosts: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/post/getPosts`,
			params: params,
		});
	},
	getPostById: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/post/getPostById`,
			params: params,
		});
	},
	getPostDepend: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/post/getPostDepend`,
			params: params,
		});
	},
	updateClick: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/post/updateClick`,
			data: data,
		});
	},
};

export default postApi;
