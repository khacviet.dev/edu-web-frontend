import axiosClient from './axiosClient';

const courseApi = {
	getCourseList: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/course/getCourseList`,
			params: params,
		});
	},
	getLessonList: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/course/getLessonList`,
			params: params,
		});
	},
	getCourse: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/course/getCourse`,
			params: params,
		});
	},
	getLessonSource: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/course/getLessonSource`,
			params: params,
		});
	},
	getSourceIdList: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/course/getSourceIdList`,
			params: params,
		});
	},
	getFeatureCourse: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/course/getFeatureCourse`,
		});
	},
	getMostBuyCourse: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/course/getMostBuyCourse`,
		});
	},

	getCourseListByTeacherId: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/course/getCourseListByTeacherId`,
			params: params,
		});
	},

	getCourseByCourseCart: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/course/courseByCourseCart`,
			params: params,
		});
	},
};

export default courseApi;
