import axiosClient from './axiosClient';

const classApi = {
	getClasses: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/class/getClasses`,
			params: params,
		});
	},
	getGradeLevel: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/class/getGradeLevel`,
		});
	},
	getSubjectList: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/class/getSubjectList`,
		});
	},
	insertRegister: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/class/insertRegister`,
			data: data,
		});
	},
};

export default classApi;
