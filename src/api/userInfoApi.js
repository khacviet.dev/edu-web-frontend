import axiosClient from './axiosClient';

const userInfoApi = {
	getUserInfo: () => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/userInfo/getUserInfo`,
		});
	},
	getCourseList: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/userInfo/getCourseList`,
			params: params,
		});
	},
	getLessonList: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/userInfo/getLessonList`,
			params: params,
		});
	},
	getLessonList: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/userInfo/getLessonList`,
			params: params,
		});
	},
	sendFeedBack: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/userInfo/sendFeedBack`,
			data: data,
		});
	},
	checkIsBuy: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/userInfo/checkIsBuy`,
			params: params,
		});
	},
};

export default userInfoApi;
