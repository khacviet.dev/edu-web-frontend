import axiosClient from './axiosClient';

const paymentApi = {
	getCourseByCourseCart: (params) => {
		return axiosClient({
			method: 'GET',
			url: `/api/v1/payment/courseByCourseCart`,
			params: params,
		});
	},
	paymentCourse: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/payment`,
			data: data,
		});
	},
};

export default paymentApi;
