import axiosClient from './axiosClient';

const authApi = {
	signin: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/auth/signin',
			data: params,
		});
	},
	signup: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/auth/signup',
			data: params,
		});
	},
	resetPassword: (params) => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/auth/reset',
			data: params,
		});
	},

	checkRecoverCode: (userId, params) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/auth/reset/recover/${userId}`,
			data: params,
		});
	},
	changePassword: (userId, params) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/auth/reset/newpassword/${userId}`,
			data: params,
		});
	},
	confirmEmail: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/api/v1/auth/confirm`,
			data: data,
		});
	},
	logout: () => {
		return axiosClient({
			method: 'POST',
			url: '/api/v1/auth/logout',
		});
	},
};

export default authApi;
