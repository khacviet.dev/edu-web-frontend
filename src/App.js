import { CssBaseline, ThemeProvider } from '@material-ui/core';
import ChangeNewPassword from 'common/pages/ChangeNewPassword';
import ChangePassword from 'common/pages/ChangePassword';
import ConfirmMail from 'common/pages/ConfirmMail';
import HomePage from 'common/pages/HomePage';
import NotFound from 'common/pages/NotFound';
import RecoverCode from 'common/pages/RecoverCode';
import ResetPassword from 'common/pages/ResetPassword';
import SignIn from 'common/pages/SignIn';
import SignUp from 'common/pages/Signup';
import UserProfile from 'common/pages/UserProfile';
import PostDetail from 'features/Blog/pages/PostDetail';
import PostList from 'features/Blog/pages/PostList';
import ClassList from 'features/Classroom/pages/ClassList';
import CourseCart from 'features/Course/pages/CourseCart';
import CourseDetail from 'features/Course/pages/CourseDetail';
import CourseList from 'features/Course/pages/CourseList';
import LearnCourse from 'features/LearnCourse';
import TeacherDetail from 'features/Teacher/pages/TeacherDetail';
import TeacherList from 'features/Teacher/pages/TeacherList';
import { Suspense } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { theme } from 'themes';
import PrivateRoute from 'utils/PrivateRoute';
import PublicRoute from 'utils/PublicRoute';
import ToastifyProvider from 'utils/ToastifyConfig';

function App() {
	return (
		<ThemeProvider theme={theme}>
			<CssBaseline />
			<Suspense>
				<BrowserRouter>
					<ToastifyProvider>
						<Switch>
							<PublicRoute component={HomePage} path='/' exact />
							<PrivateRoute component={CourseCart} path='/course-cart' exact />
							<PrivateRoute component={UserProfile} path='/profile' exact />
							<PrivateRoute component={CourseList} path='/mycourse' exact />
							<PrivateRoute
								component={ChangeNewPassword}
								path='/changepassword'
								exact
							/>
							<PublicRoute component={CourseList} path='/course' exact />
							<PublicRoute
								component={CourseDetail}
								path='/coursedetail'
								exact
							/>
							<PublicRoute component={ClassList} path='/class' exact />
							<PublicRoute component={PostList} path='/post' exact />
							<PublicRoute
								component={PostDetail}
								path='/postdetail/:postId'
								exact
							/>

							<PublicRoute component={LearnCourse} path='/learn' exact />
							<PublicRoute component={TeacherList} path='/teachers' exact />
							<PublicRoute
								component={TeacherDetail}
								path='/teachers/:userId'
								exact
							/>
							<PublicRoute
								restricted={true}
								component={SignIn}
								path='/signin'
								exact
							/>
							<PublicRoute
								restricted={true}
								component={SignUp}
								path='/signup'
								exact
							/>
							<PublicRoute
								restricted={true}
								component={ResetPassword}
								path='/reset'
								exact
							/>
							<PublicRoute
								restricted={true}
								component={RecoverCode}
								path='/reset/recover/:userId'
								exact
							/>
							<PublicRoute
								restricted={true}
								component={ChangePassword}
								path='/reset/newpassword/:userId'
								exact
							/>
							<PublicRoute
								restricted={true}
								component={ConfirmMail}
								path='/confirm'
								exact
							/>
							<Route component={NotFound} />
						</Switch>
					</ToastifyProvider>
				</BrowserRouter>
			</Suspense>
		</ThemeProvider>
	);
}

export default App;
